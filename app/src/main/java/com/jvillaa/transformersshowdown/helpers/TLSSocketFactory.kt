package com.jvillaa.transformersshowdown.helpers

import java.net.InetAddress
import java.net.Socket
import java.security.KeyStore
import javax.net.ssl.*

/**
 * @author Juan Camilo Villa
 *
 * This TLS Custom Factory is used to generate our HttpClient.
 * This approach is used to make networking calls in pre-lollipop devices
 * because it was failing due to Client-Server handshake error & TLS protocols.
 *
 * See https://stackoverflow.com/questions/51020874/failure-response-retrofit-2 for more information
 */
class TLSSocketFactory : SSLSocketFactory() {

    private val delegate: SSLSocketFactory
    private var trustManagers: Array<TrustManager>? = null

    override fun createSocket(): Socket? {
        return enableTLSOnSocket(delegate.createSocket())
    }

    override fun createSocket(s: Socket?, host: String?, port: Int, autoClose: Boolean): Socket? {
        return enableTLSOnSocket(delegate.createSocket(s, host, port, autoClose))
    }

    override fun createSocket(host: String?, port: Int): Socket? {
        return enableTLSOnSocket(delegate.createSocket(host, port))
    }

    override fun createSocket(host: String?, port: Int, localHost: InetAddress?, localPort: Int): Socket? {
        return enableTLSOnSocket(delegate.createSocket(host, port, localHost, localPort))
    }

    override fun createSocket(host: InetAddress?, port: Int): Socket? {
        return enableTLSOnSocket(delegate.createSocket(host, port))
    }

    override fun createSocket(
        address: InetAddress?,
        port: Int,
        localAddress: InetAddress?,
        localPort: Int
    ): Socket? {
        return enableTLSOnSocket(delegate.createSocket(address, port, localAddress, localPort))
    }

    override fun getDefaultCipherSuites(): Array<String> {
        return delegate.defaultCipherSuites
    }

    override fun getSupportedCipherSuites(): Array<String> {
        return delegate.supportedCipherSuites
    }

    private fun enableTLSOnSocket(socket: Socket?): Socket? {
        if (socket != null && socket is SSLSocket) {
            socket.enabledProtocols = arrayOf("TLSv1.2")
        }
        return socket
    }

    private fun generateTrustManagers() {
        val trustManagerFactory: TrustManagerFactory =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(null as KeyStore?)
        val trustManagers: Array<TrustManager> = trustManagerFactory.trustManagers
        check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
            ("Unexpected default trust managers:"
                    + trustManagers.contentToString())
        }
        this.trustManagers = trustManagers
    }

    fun getTrustManager(): X509TrustManager? {
        return trustManagers!![0] as X509TrustManager
    }

    init {
        generateTrustManagers()
        val context: SSLContext = SSLContext.getInstance("TLS")
        context.init(null, null, null)
        delegate = context.socketFactory
    }
}