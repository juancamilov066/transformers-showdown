package com.jvillaa.transformersshowdown

import android.app.Application
import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * The main entry point of the app
 * Here we enable Hilt as our Dependencies Manager
 */
@HiltAndroidApp
class TransformersShowdownApplication : Application() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    /**
     * In this function we set up the initial Theme for our app
     * Transformers: Showdown supports Night/Day themes.
     * Also we init Stetho to monitor our network traffic, storage, etc.
     */
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)

        val isNightModeSet = isNightModeSet()
        if (isNightModeSet != null) {
            val isNightModeEnabled = isNightModeSet.toBoolean()
            AppCompatDelegate.setDefaultNightMode(if (isNightModeEnabled) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        } else {
            val isSystemNight = isSystemNight()
            setNightModeEnabled(isSystemNight)
            AppCompatDelegate.setDefaultNightMode(if (isSystemNight) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    /**
     * This function is used to know the current Day/Night config
     *
     * @return Boolean tells if system is night or day mode
     */
    private fun isSystemNight(): Boolean {
        return when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_YES -> true
            else -> false
        }
    }

    /**
     * This function tells if we previously set any Day/Night config
     *
     * @return String? current Day/Night config
     */
    private fun isNightModeSet(): String? {
        return sharedPreferences.getString("NIGHT_MODE_PREF", null)
    }

    /**
     * This function stores our current Day/Night config
     * Anytime the app starts we'll know the last Day/Night config value to be set
     *
     * @param isEnabled is Night enabled or not
     */
    private fun setNightModeEnabled(isEnabled: Boolean) {
        sharedPreferences.edit().putString("NIGHT_MODE_PREF", isEnabled.toString()).apply()
    }
}