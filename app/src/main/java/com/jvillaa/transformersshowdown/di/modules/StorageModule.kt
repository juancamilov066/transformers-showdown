package com.jvillaa.transformersshowdown.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

/**
 * @author Juan Camilo Villa
 *
 * Dagger Hilt module to build local storage dependencies
 */
@Module
@InstallIn(ApplicationComponent::class, ActivityComponent::class)
class StorageModule {

    /**
     * This function provides our main local data source
     * used this is because we're only saving a string (token) along the entire
     * application
     *
     * @return SharedPreferences
     * @param context Base app context
     */
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    companion object {
        private const val PREFS_NAME = "TransformersShowdownPreferences"
    }

}