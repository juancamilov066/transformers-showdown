package com.jvillaa.transformersshowdown.di.modules

import com.jvillaa.transformersshowdown.data.datasource.local.auth.AuthLocalDataSource
import com.jvillaa.transformersshowdown.data.datasource.local.auth.AuthLocalDataSourceImpl
import com.jvillaa.transformersshowdown.data.datasource.remote.auth.AuthRemoteDataSource
import com.jvillaa.transformersshowdown.data.datasource.remote.auth.AuthRemoteDataSourceImpl
import com.jvillaa.transformersshowdown.data.datasource.remote.transformers.TransformersRemoteDataSource
import com.jvillaa.transformersshowdown.data.datasource.remote.transformers.TransformersRemoteDataSourceImpl
import com.jvillaa.transformersshowdown.data.repository.AuthRepositoryImpl
import com.jvillaa.transformersshowdown.data.repository.TransformersRepositoryImpl
import com.jvillaa.transformersshowdown.domain.repository.AuthRepository
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

/**
 * @author Juan Camilo Villa
 *
 * Dagger Hilt module to build app related dependencies
 * (Repositories and DataSources) to inject along the dependencies tree
 */
@Module
@InstallIn(ApplicationComponent::class)
abstract class AppModule {

    @Binds
    abstract fun bindAuthRemoteDataSource(authRemoteDataSourceImpl: AuthRemoteDataSourceImpl): AuthRemoteDataSource

    @Binds
    abstract fun bindAuthLocalDataSource(authLocalDataSourceImpl: AuthLocalDataSourceImpl): AuthLocalDataSource

    @Binds
    abstract fun bindTransformersRemoteDataSource(transformersRemoteDataSourceImpl: TransformersRemoteDataSourceImpl): TransformersRemoteDataSource

    @Binds
    abstract fun bindAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Binds
    abstract fun bindTransformersRepository(transformersRepositoryImpl: TransformersRepositoryImpl): TransformersRepository

}