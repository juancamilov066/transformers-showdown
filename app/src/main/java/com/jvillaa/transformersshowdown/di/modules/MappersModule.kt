package com.jvillaa.transformersshowdown.di.modules

import com.jvillaa.transformersshowdown.data.mappers.Mapper
import com.jvillaa.transformersshowdown.data.mappers.TransformersObjectMapper
import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

/**
 * @author Juan Camilo Villa
 *
 * Dagger Hilt module to build object mappers used to transform
 * objects between domain and data layers
 */
@Module
@InstallIn(ApplicationComponent::class)
class MappersModule {

    @Provides
    fun provideTransformersObjectMapper(): Mapper<ApiTransformer, DomainTransformer> =
        TransformersObjectMapper

}