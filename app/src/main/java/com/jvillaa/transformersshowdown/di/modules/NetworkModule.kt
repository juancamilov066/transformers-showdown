package com.jvillaa.transformersshowdown.di.modules

import android.content.SharedPreferences
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jvillaa.transformersshowdown.data.api.AuthApi
import com.jvillaa.transformersshowdown.data.api.TransformerApi
import com.jvillaa.transformersshowdown.data.helpers.AuthInterceptor
import com.jvillaa.transformersshowdown.helpers.TLSSocketFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

/**
 * @author Juan Camilo Villa
 *
 * Dagger Hilt module to build networking dependencies
 */
@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    /**
     * Provides the http client to be used by Retrofit library
     * Note that here we use a custom TLS Socket Factory.
     *
     * @see TLSSocketFactory
     * @param authInterceptor Request interceptor to append Auth Token
     * @return OkHttpClient
     */
    @Provides
    @Singleton
    fun provideHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
        val tlsSocketFactory = TLSSocketFactory()
        val trustManager = tlsSocketFactory.getTrustManager()

        if (trustManager != null) {
            return OkHttpClient
                .Builder()
                .sslSocketFactory(tlsSocketFactory, trustManager)
                .addInterceptor(authInterceptor)
                .addNetworkInterceptor(StethoInterceptor())
                .build()
        }
        return OkHttpClient
            .Builder()
            .addInterceptor(authInterceptor)
            .addNetworkInterceptor(StethoInterceptor())
            .build()
    }

    /**
     * Provides the request interceptor for our Retrofit client
     * @param sharedPreferences Android Shared Preferences to get out cached token
     * @return AuthInterceptor
     */
    @Provides
    @Singleton
    fun providesAuthInterceptor(sharedPreferences: SharedPreferences): AuthInterceptor {
        return AuthInterceptor(sharedPreferences)
    }

    /**
     * Provides our Retrofit Networking client
     * For this implementation we use ScalarsConverterFactory since the
     * /allspark request returns a plain String
     *
     * @param okHttpClient Base client for Retrofit
     * @return Retrofit
     */
    @Provides
    @Singleton
    fun provideRetrofitClient(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://transformers-api.firebaseapp.com/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    /**
     * Provides our endpoint to make auth related calls to the server
     * @param retrofit Retrofit instance
     * @return AuthApi
     * @see AuthApi
     */
    @Provides
    @Singleton
    fun provideAuthService(retrofit: Retrofit): AuthApi = retrofit.create(AuthApi::class.java)

    /**
     * Provides our endpoint to make transformers related calls to the server
     * @param retrofit Retrofit instance
     * @return TransformerApi
     * @see TransformerApi
     */
    @Provides
    @Singleton
    fun provideTransformersService(retrofit: Retrofit): TransformerApi =
        retrofit.create(TransformerApi::class.java)
}