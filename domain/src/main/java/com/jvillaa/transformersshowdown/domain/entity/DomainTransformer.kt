package com.jvillaa.transformersshowdown.domain.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Juan Camilo Villa
 *
 * This domain entity models a Transformer
 *
 * @param id Transformer id, populated only when it comes from server
 * @param name Transformer name
 * @param team Transformer team: A or D
 * @param strength Transformer strength: 1 - 10
 * @param intelligence Transformer intelligence: 1 - 10
 * @param speed Transformer speed: 1 - 10
 * @param endurance Transformer endurance: 1 - 10
 * @param rank Transformer rank: 1 - 10
 * @param courage Transformer courage: 1 - 10
 * @param firepower Transformer firepower: 1 - 10
 * @param skill Transformer skill: 1 - 10
 * @param teamIcon Team icon URL
 */
data class DomainTransformer(
    var id: String?,
    var name: String,
    var team: String,
    var strength: Int,
    var intelligence: Int,
    var speed: Int,
    var endurance: Int,
    var rank: Int,
    var courage: Int,
    var firepower: Int,
    var skill: Int,
    var teamIcon: String?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        id = parcel.readString(),
        name = parcel.readString()!!,
        team = parcel.readString()!!,
        strength = parcel.readInt(),
        intelligence = parcel.readInt(),
        speed = parcel.readInt(),
        endurance = parcel.readInt(),
        rank = parcel.readInt(),
        courage = parcel.readInt(),
        firepower = parcel.readInt(),
        skill = parcel.readInt(),
        teamIcon = parcel.readString()
    )

    fun getOverall() = strength + intelligence + speed + endurance + firepower

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(team)
        parcel.writeInt(strength)
        parcel.writeInt(intelligence)
        parcel.writeInt(speed)
        parcel.writeInt(endurance)
        parcel.writeInt(rank)
        parcel.writeInt(courage)
        parcel.writeInt(firepower)
        parcel.writeInt(skill)
        parcel.writeString(teamIcon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DomainTransformer> {
        override fun createFromParcel(parcel: Parcel): DomainTransformer {
            return DomainTransformer(parcel)
        }

        override fun newArray(size: Int): Array<DomainTransformer?> {
            return arrayOfNulls(size)
        }
    }
}