package com.jvillaa.transformersshowdown.domain.entity

/**
 * @author Juan Camilo Villa
 *
 * This domain entity models the result of a battle
 *
 * @param fightsNumber Number of fights the battle had
 * @param winnerTeam Winner faction, T in case of Tie
 * @param winnerTransformers List of winners of each fight
 * @param survivors Transformers who didn't had the chance of fight in this battle
 */
data class DomainBattleResults (
    val fightsNumber: Int,
    val winnerTeam: String,
    val winnerTransformers: List<DomainTransformer>,
    val survivors: List<DomainTransformer>
)