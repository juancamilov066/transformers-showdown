package com.jvillaa.transformersshowdown.domain.repository

import com.jvillaa.transformersshowdown.domain.helpers.Result

/**
 * @author Juan Camilo Villa
 *
 * This interface is the contract for all the possible auth related operations
 */
interface AuthRepository {
    /**
     * This function gets an auth token
     *
     * @return Result with auth token
     */
    suspend fun retrieveAuthToken(): Result<String>

    /**
     * This function saves a token
     *
     * @param token Auth token to be saved
     * @return Success or error in the operation
     */
    suspend fun saveAuthToken(token: String): Result<Unit>
}