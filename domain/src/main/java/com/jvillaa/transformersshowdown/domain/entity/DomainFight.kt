package com.jvillaa.transformersshowdown.domain.entity

/**
 * @author Juan Camilo Villa
 *
 * This entity describes a single fight between transformers
 *
 * @param autobot Autobot contender
 * @param decepticon Decepticon contender
 */
data class DomainFight (
    val autobot: DomainTransformer,
    val decepticon: DomainTransformer
)