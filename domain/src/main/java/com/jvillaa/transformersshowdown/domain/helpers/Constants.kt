package com.jvillaa.transformersshowdown.domain.helpers

/**
 * @author Juan Camilo Villa
 *
 * Constants used along the domain module and it's implementers
 */
object Constants {
    const val PREF_TOKEN = "PREF_TOKEN"
}