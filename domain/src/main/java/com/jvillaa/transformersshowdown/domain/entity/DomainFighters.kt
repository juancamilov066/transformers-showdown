package com.jvillaa.transformersshowdown.domain.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Juan Camilo Villa
 *
 * This entity describes a fight list between transformers
 * Implements parcelable to be shared between views
 *
 * @param autobots Autobots contenders
 * @param decepticons Decepticons contenders
 */
data class DomainFighters (
    var autobots: List<DomainTransformer>,
    var decepticons: List<DomainTransformer>
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(DomainTransformer)!!.toList(),
        parcel.createTypedArrayList(DomainTransformer)!!.toList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(autobots)
        parcel.writeTypedList(decepticons)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DomainFighters> {
        override fun createFromParcel(parcel: Parcel): DomainFighters {
            return DomainFighters(parcel)
        }

        override fun newArray(size: Int): Array<DomainFighters?> {
            return arrayOfNulls(size)
        }
    }
}