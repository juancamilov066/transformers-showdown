package com.jvillaa.transformersshowdown.domain.repository

import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result

/**
 * @author Juan Camilo Villa
 *
 * This interface is the contract for all the possible transformers related operations
 */
interface TransformersRepository {
    /**
     * This function gets all created transformers
     *
     * @return Result with transformers
     */
    suspend fun getTransformers(): Result<List<DomainTransformer>>

    /**
     * This function saves a transformer
     *
     * @param domainTransformer transformer to be saved
     * @return Result with saved transformer
     */
    suspend fun saveTransformer(domainTransformer: DomainTransformer): Result<DomainTransformer>

    /**
     * This function deletes a transformer by their id
     *
     * @param id Id of the transformer to be deleted
     * @return Success or error in the operation
     */
    suspend fun deleteTransformerById(id: String): Result<Unit>

    /**
     * This function edits a transformer
     *
     * @param domainTransformer transformer to be edited
     * @return Result with edited transformer
     */
    suspend fun editTransformer(domainTransformer: DomainTransformer): Result<DomainTransformer>
}