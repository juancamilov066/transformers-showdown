package com.jvillaa.transformersshowdown.domain.helpers

/**
 * @author Juan Camilo Villa
 *
 * This sealed class contains all the responses status coming from either remote or local
 * data sources. Helps to standardize and know the real status of an operation
 */
sealed class Result<out T> {
    /**
     * Used to represent an OK state of an operation
     *
     * @param data Data to be returned into the result
     */
    data class Success<out T>(val data: T): Result<T>()

    /**
     * Used to represent an Error state of an operation
     *
     * @param error Cause of the error
     */
    data class Error(val error: Exception): Result<Nothing>()
}