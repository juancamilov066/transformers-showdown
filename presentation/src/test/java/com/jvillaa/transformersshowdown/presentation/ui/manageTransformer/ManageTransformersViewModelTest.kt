package com.jvillaa.transformersshowdown.presentation.ui.manageTransformer

import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import com.jvillaa.transformersshowdown.presentation.CoroutineTestRule
import com.jvillaa.transformersshowdown.presentation.InstantExecutorExtension
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantExecutorExtension::class)
class ManageTransformersViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    //SUT
    private lateinit var manageTransformerViewModel: ManageTransformerViewModel

    //Collaborators
    private val transformersRepository: TransformersRepository = mockk()

    @BeforeEach
    fun init() {
        clearMocks(transformersRepository)
        manageTransformerViewModel = ManageTransformerViewModel(transformersRepository)
    }

    @Nested
    @DisplayName(value = "Given transformer")
    inner class TransformersTests {
        @Test
        @DisplayName("When it tries to create a transformer, should call its repository")
        fun saveTransformer() {
            val transformer: DomainTransformer = mockk()
            coEvery {
                transformersRepository.saveTransformer(transformer)
            } returns mockk()
            manageTransformerViewModel.saveTransformer(transformer)
            coVerify { transformersRepository.saveTransformer(transformer) }
        }

        @Test
        @DisplayName("When it tries to edit a transformer, should call its repository")
        fun editTransformer() {
            val transformer: DomainTransformer = mockk()
            coEvery {
                transformersRepository.editTransformer(transformer)
            } returns mockk()
            manageTransformerViewModel.editTransformer(transformer)
            coVerify { transformersRepository.editTransformer(transformer) }
        }
    }

}