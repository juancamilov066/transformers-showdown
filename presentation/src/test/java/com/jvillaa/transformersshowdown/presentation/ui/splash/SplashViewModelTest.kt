package com.jvillaa.transformersshowdown.presentation.ui.splash

import com.jvillaa.transformersshowdown.domain.repository.AuthRepository
import com.jvillaa.transformersshowdown.presentation.CoroutineTestRule
import com.jvillaa.transformersshowdown.presentation.InstantExecutorExtension
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantExecutorExtension::class)
class SplashViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    //SUT
    private lateinit var splashViewModel: SplashViewModel

    //Collaborators
    private val authRepository: AuthRepository = mockk()

    @BeforeEach
    fun init() {
        clearMocks(authRepository)
        splashViewModel = SplashViewModel(authRepository)
    }

    @Nested
    @DisplayName(value = "Given auth guidelines")
    inner class SplashTests {
        @Test
        @DisplayName("When it tries to retrieve token, authRepository is called")
        fun retrieveToken() = coroutineTestRule.testDispatcher.runBlockingTest {
            coEvery {
                authRepository.retrieveAuthToken()
            } returns mockk()
            splashViewModel.login()
            coVerify { authRepository.retrieveAuthToken() }
        }

        @Test
        @DisplayName("When it has a token and tries to save it, authRepository is called")
        fun saveToken() = coroutineTestRule.testDispatcher.runBlockingTest {
            val token = "ABCD1234"
            coEvery {
                authRepository.saveAuthToken(token)
            } returns mockk()
            splashViewModel.saveToken(token)
            coVerify { authRepository.saveAuthToken(token) }
        }
    }

}