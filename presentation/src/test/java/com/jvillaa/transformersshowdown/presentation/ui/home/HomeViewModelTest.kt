package com.jvillaa.transformersshowdown.presentation.ui.home

import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import com.jvillaa.transformersshowdown.presentation.CoroutineTestRule
import com.jvillaa.transformersshowdown.presentation.InstantExecutorExtension
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantExecutorExtension::class)
class HomeViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    //SUT
    private lateinit var homeViewModel: HomeViewModel

    //Collaborators
    private val transformersRepository: TransformersRepository = mockk()

    @BeforeEach
    fun init() {
        clearMocks(transformersRepository)
        homeViewModel = HomeViewModel(transformersRepository)
    }

    @Nested
    @DisplayName(value = "Given transformers")
    inner class TransformersTests {
        @Test
        @DisplayName("When it tries to delete a transformer by id, should call its repository")
        fun getTransformersList() {
            val id = "id"
            coEvery {
                transformersRepository.deleteTransformerById(id)
            } returns mockk()
            homeViewModel.deleteTransformerById(id)
            coVerify { transformersRepository.deleteTransformerById(id) }
        }
    }

}