package com.jvillaa.transformersshowdown.presentation.ui.transformersList

import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import com.jvillaa.transformersshowdown.presentation.CoroutineTestRule
import com.jvillaa.transformersshowdown.presentation.InstantExecutorExtension
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(InstantExecutorExtension::class)
class TransformersListViewModelTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    //SUT
    private lateinit var transformersListViewModel: TransformersListViewModel

    //Collaborators
    private val transformersRepository: TransformersRepository = mockk()

    @BeforeEach
    fun init() {
        clearMocks(transformersRepository)
        transformersListViewModel = TransformersListViewModel(transformersRepository)
    }

    @Nested
    @DisplayName(value = "Given transformers")
    inner class TransformersTests {
        @Test
        @DisplayName("When it tries to download the transformers list, should call its repository")
        fun getTransformersList() {
            coEvery {
                transformersRepository.getTransformers()
            } returns mockk()
            transformersListViewModel.fetchTransformers()
            coVerify { transformersRepository.getTransformers() }
        }
    }

}