package com.jvillaa.transformersshowdown.presentation.custom

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutSkillPickerBinding
import com.jvillaa.transformersshowdown.presentation.helpers.OnSkillChangedListener

/**
 * @author Juan Camilo Villa
 */
class TransformersSkillSelector @JvmOverloads constructor(
    context: Context,
    private val attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding: LayoutSkillPickerBinding

    private var currentValue = 1
    private var maxValue = 10
    private var minValue = 1
    private var title: String? = null
    private lateinit var onSkillChangedListener: OnSkillChangedListener

    init {
        val view = inflate(context, R.layout.layout_skill_picker, this)
        binding = LayoutSkillPickerBinding.bind(view)
        initView()
    }

    private fun initView() {
        attrs?.let {
            val typedArray =
                context.obtainStyledAttributes(attrs, R.styleable.TransformersSkillSelector)
            minValue = typedArray.getInt(
                R.styleable.TransformersSkillSelector_minValue,
                1
            )
            maxValue = typedArray.getInt(
                R.styleable.TransformersSkillSelector_maxValue,
                10
            )
            title = typedArray.getString(R.styleable.TransformersSkillSelector_title)
            binding.skillIndicatorLabel.text = title
            typedArray.recycle()
        }

        binding.currentValueInput.setText(minValue.toString())
        binding.increaseButton.setOnClickListener {
            if (canValueBeChanged(true)) {
                onValueChange(true)
            }
        }

        binding.decreaseButton.setOnClickListener {
            if (canValueBeChanged(false)) {
                onValueChange(false)
            }
        }
    }

    private fun canValueBeChanged(isIncreasing: Boolean): Boolean {
        if (isIncreasing && currentValue == maxValue || !isIncreasing && currentValue == minValue) {
            return false
        }
        return true
    }

    private fun toggleButtonActivation() {
        binding.increaseButton.isEnabled = currentValue != minValue
        binding.increaseButton.isEnabled = currentValue != maxValue
    }

    private fun onValueChange(isIncreasing: Boolean) {
        val previousValue = currentValue
        if (isIncreasing) currentValue++ else currentValue--
        binding.currentValueInput.setText(currentValue.toString())
        onSkillChangedListener.onSkillValueChanged(this, previousValue, currentValue)
        toggleButtonActivation()
    }

    fun reset() {
        currentValue = 1
        binding.currentValueInput.setText(currentValue.toString())
    }

    fun setCurrentValue(currentValue: Int) {
        if (currentValue > maxValue || currentValue <minValue) {
            throw IllegalStateException("Current value out of bounds")
        }

        this.currentValue = currentValue
        binding.currentValueInput.setText(currentValue.toString())
    }

    fun setOnValueChangedListener(listener: OnSkillChangedListener) {
        onSkillChangedListener = listener
    }

}