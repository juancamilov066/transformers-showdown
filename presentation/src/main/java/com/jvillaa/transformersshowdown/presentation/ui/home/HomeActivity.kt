package com.jvillaa.transformersshowdown.presentation.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.ActivityHomeBinding
import com.jvillaa.transformersshowdown.presentation.helpers.*
import com.jvillaa.transformersshowdown.presentation.ui.BaseActivity
import com.jvillaa.transformersshowdown.presentation.ui.detail.DetailFragment
import com.jvillaa.transformersshowdown.presentation.ui.manageTransformer.ManageTransformerActivity
import com.jvillaa.transformersshowdown.presentation.ui.transformersList.TransformersListFragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Juan Camilo Villa
 *
 * This activity contains the transformers list and the transformer detail depending on the
 * device's size, also is the responsible of handling the theme change Day/Night
 */
@AndroidEntryPoint
class HomeActivity : BaseActivity(), OnTransformerEditionListener {

    private lateinit var indeterminateDialogHelper: IndeterminateDialogHelper
    private lateinit var binding: ActivityHomeBinding

    private lateinit var transformerInteractionListener: OnTransformerInteractionListener
    private lateinit var transformerDetailInteractionListener: OnTransformerDetailInteractionListener

    private val homeViewModel: HomeViewModel by viewModels()
    private val transformerDetailDialogHelper: TransformerDetailDialogHelper by lazy {
        TransformerDetailDialogHelper(this, this)
    }

    private var shouldRenderTransformerDetail = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.bind(View.inflate(this, R.layout.activity_home, null))
        setContentView(binding.root)
        setToolbar()

        shouldRenderTransformerDetail = binding.transformerDetailContainer != null
        indeterminateDialogHelper = IndeterminateDialogHelper(this)

        setContent()
        setMenu()
        observeLiveData()
    }

    /**
     * Sets the navigation drawer options
     */
    @SuppressLint("RtlHardcoded")
    private fun setMenu() {
        binding.navigationViewIncludedLayout.let {
            it.dayThemeLabel.setOnClickListener {
                if (!isNightModeEnabled()) return@setOnClickListener
                setNightModeEnabled(false)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                recreate()
            }
            it.nightThemeLabel.setOnClickListener {
                if (isNightModeEnabled()) return@setOnClickListener
                setNightModeEnabled(true)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                recreate()
            }
            it.followSystemThemeLabel.setOnClickListener {
                setNightModeEnabled(null)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                recreate()
            }
            it.creditsLabel.setOnClickListener {
                binding.homeDrawerLayout.closeDrawer(Gravity.LEFT)
                CreditsDialogHelper(this).getDialog().show()
            }
        }
    }

    /**
     * Observes the view model's live data objects
     */
    private fun observeLiveData() {
        val dialog = indeterminateDialogHelper.getDialog()
        homeViewModel.deleteTransformersLiveData.observe(this, {
            when (it) {
                is Status.Loading -> {
                    dialog.show()
                }
                is Status.Success -> {
                    transformerInteractionListener.updateTransformers()
                    dialog.dismiss()

                    if (shouldRenderTransformerDetail) {
                        binding.selectTransformerLabel!!.isVisible = true
                        binding.transformerDetailContainer!!.isVisible = false
                    }
                }
                is Status.Error -> {
                    dialog.dismiss()
                    showMessage(getString(R.string.request_error))
                }
            }
        })
    }

    /**
     * Sets the toolbar information
     */
    private fun setToolbar() {
        setSupportActionBar(binding.toolbarIncludedLayout.baseToolbar)
        binding.toolbarIncludedLayout.toolbarTitle.text = getString(R.string.app_main_name)
    }

    /**
     * Sets the listener who is going to react to update redirection events
     *
     * @param listener Interface to communicate
     */
    fun setTransformersUpdateListener(listener: OnTransformerInteractionListener) {
        transformerInteractionListener = listener
    }

    /**
     * Sets the listener who is going to react to detail events
     *
     * @param listener Interface to communicate
     */
    fun setDetailInteractionListener(listener: OnTransformerDetailInteractionListener) {
        transformerDetailInteractionListener = listener
    }

    /**
     * Sets the listener who is going to react to detail events
     *
     * @param domainTransformer Transformer selected from list
     */
    fun onTransformerSelected(domainTransformer: DomainTransformer) {
        if (shouldRenderTransformerDetail) {
            binding.selectTransformerLabel!!.isVisible = false
            binding.transformerDetailContainer!!.isVisible = true

            transformerDetailInteractionListener.onShowTransformerDetail(domainTransformer)
        } else {
            val dialog = transformerDetailDialogHelper.getDialog(domainTransformer)
            dialog.show()
        }
    }

    /**
     * This function calls the deletion event from any child
     *
     * @param transformer Transformer to be deleted
     */
    fun deleteTransformer(transformer: DomainTransformer) {
        homeViewModel.deleteTransformerById(transformer.id!!)
    }

    /**
     * This function calls the edition event from any child
     *
     * @param transformer Transformer to be deleted
     */
    fun editTransformer(transformer: DomainTransformer) {
        val intent = Intent(this, ManageTransformerActivity::class.java)
        intent.putExtra(Constants.TRANSFORMER_TO_UPDATE_ARG, transformer)
        startActivityForResult(
            intent,
            Constants.UPDATE_ACTIVITY_CODE
        )
    }

    /**
     * This function sets views initial info
     */
    private fun setContent() {
        if (shouldRenderTransformerDetail) {
            val transformersListFragment = TransformersListFragment.newInstance()
            attachFragment(
                R.id.listContainer,
                transformersListFragment,
                "TRANSFORMERS_LIST_FRAGMENT"
            )

            val detailFragment = DetailFragment.newInstance()
            attachFragment(R.id.transformerDetailContainer, detailFragment, "DETAIL_FRAGMENT")
        } else {
            val transformersListFragment = TransformersListFragment.newInstance()
            attachFragment(
                R.id.listContainer,
                transformersListFragment,
                "TRANSFORMERS_LIST_FRAGMENT"
            )
        }
    }

    /**
     * This method is used to simplify Fragment Attachment to this activity
     *
     * @param containerId FrameLayout id where's going to be the fragment
     * @param fragmentInstance Fragment to show
     * @param tag Fragment tag reference
     */
    private fun attachFragment(containerId: Int, fragmentInstance: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(containerId, fragmentInstance, tag)
            .commit()
    }

    /**
     * This function is used to know the current Day/Night config saved value
     *
     * @return If Day/Night is enabled
     */
    private fun isNightModeEnabled(): Boolean {
        val isPreferenceSet = sharedPreferences.getString("NIGHT_MODE_PREF", null) != null
        return if (isPreferenceSet) {
            sharedPreferences.getString("NIGHT_MODE_PREF", "false").toBoolean()
        } else {
            setNightModeEnabled(isSystemNight())
            isSystemNight()
        }
    }

    /**
     * This function saves Day/Night config
     *
     * @param isEnabled Day/Night value to save
     */
    private fun setNightModeEnabled(isEnabled: Boolean?) {
        if (isEnabled == null) {
            sharedPreferences.edit().putString("NIGHT_MODE_PREF", isEnabled).apply()
        } else {
            sharedPreferences.edit().putString("NIGHT_MODE_PREF", isEnabled.toString()).apply()
        }
    }

    /**
     * This function is used to know the current Day/Night system config
     *
     * @return If Day/Night is enabled
     */
    private fun isSystemNight(): Boolean {
        return when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_YES -> true
            else -> false
        }
    }

    /**
     * This methods handles when the list needs to be updated, due to an addition or a edition of any
     * transformer
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Constants.UPDATE_ACTIVITY_CODE) {
            val shouldUpdate = data!!.getBooleanExtra(Constants.SHOULD_UPDATE_ARG, false)
            if (shouldUpdate) {
                transformerInteractionListener.updateTransformers()

                if (shouldRenderTransformerDetail) {
                    binding.selectTransformerLabel!!.isVisible = true
                    binding.transformerDetailContainer!!.isVisible = false
                }
            }
        }
    }

    /**
     * This function reacts to a button delete action
     *
     * @param transformer Transformer to delete
     */
    override fun onDeleteTransformerClick(transformer: DomainTransformer) {
        deleteTransformer(transformer)
    }

    /**
     * This function reacts to a button edit action
     *
     * @param transformer Transformer to edit
     */
    override fun onEditTransformerClick(transformer: DomainTransformer) {
        editTransformer(transformer)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    @SuppressLint("RtlHardcoded")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.drawerMenu) {
            binding.homeDrawerLayout.openDrawer(Gravity.LEFT)
            return true
        }
        return false
    }
}