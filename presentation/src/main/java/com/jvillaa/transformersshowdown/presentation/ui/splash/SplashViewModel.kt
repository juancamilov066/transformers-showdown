package com.jvillaa.transformersshowdown.presentation.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.domain.repository.AuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @author Juan Camilo Villa
 *
 * This view model handles needed domain interactions
 *
 * @param authRepository needed to save and request auth token
 */
class SplashViewModel @ViewModelInject constructor(
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _authTokenLiveData = MediatorLiveData<Status<String>>()
    val authTokenLiveData = _authTokenLiveData

    private val _tokenStorageLiveData = MediatorLiveData<Status<Unit>>()
    val tokenStorageLiveData = _tokenStorageLiveData

    /**
     * Saves auth token and post if the operation was successful
     *
     * @param token Token to be saved
     */
    fun saveToken(token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = authRepository.saveAuthToken(token)) {
                is Result.Success -> {
                    _tokenStorageLiveData.postValue(Status.Success(result.data))
                }
                is Result.Error -> {
                    _tokenStorageLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

    /**
     * Logs into the application and post the result
     */
    fun login() {
        _authTokenLiveData.postValue(Status.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = authRepository.retrieveAuthToken()) {
                is Result.Success -> {
                    _authTokenLiveData.postValue(Status.Success(result.data))
                }
                is Result.Error -> {
                    _authTokenLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

}