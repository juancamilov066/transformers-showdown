package com.jvillaa.transformersshowdown.presentation.helpers

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutTransformerDetailBinding

/**
 * @author Juan Camilo Villa
 *
 * This dialog helper simplifies the ErrorDialogHelper building process
 *
 * @param context used to inflate the custom view
 */
class TransformerDetailDialogHelper(
    private val context: Context,
    private val onTransformerEditionListener: OnTransformerEditionListener
) {

    fun getDialog(transformer: DomainTransformer): AlertDialog {
        val binding = LayoutTransformerDetailBinding.bind(
            View.inflate(
                context,
                R.layout.layout_transformer_detail,
                null
            )
        )

        val dialog = AlertDialog
            .Builder(context)
            .setView(binding.root)
            .create()

        setInfo(dialog, binding, transformer)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return dialog
    }

    private fun setInfo(
        dialog: AlertDialog,
        binding: LayoutTransformerDetailBinding,
        transformer: DomainTransformer
    ) {
        if (transformer.team == "A") {
            binding.teamLabel.text = context.getString(R.string.team_autobots)
            binding.teamBackgroundIcon.setImageResource(R.drawable.ic_autobot)
        } else {
            binding.teamLabel.text = context.getString(R.string.team_decepticons)
            binding.teamBackgroundIcon.setImageResource(R.drawable.ic_decepticon)
        }

        binding.transformerNameLabel.text = transformer.name

        binding.courageLabel.text =
            context.getString(R.string.courage_value_label, transformer.courage.toString())
        binding.speedLabel.text =
            context.getString(R.string.speed_value_label, transformer.speed.toString())
        binding.intelligenceLabel.text = context.getString(
            R.string.intelligence_value_label,
            transformer.intelligence.toString()
        )
        binding.strengthLabel.text =
            context.getString(R.string.strength_value_label, transformer.strength.toString())
        binding.enduranceLabel.text =
            context.getString(R.string.endurance_value_label, transformer.endurance.toString())
        binding.skillLabel.text =
            context.getString(R.string.skill_value_label, transformer.skill.toString())
        binding.rankLabel.text =
            context.getString(R.string.rank_value_label, transformer.rank.toString())
        binding.firepowerLabel.text =
            context.getString(R.string.firepower_value_label, transformer.firepower.toString())

        binding.editButton.setOnClickListener {
            dialog.dismiss()
            onTransformerEditionListener.onEditTransformerClick(transformer)
        }

        binding.deleteButton.setOnClickListener {
            dialog.dismiss()
            onTransformerEditionListener.onDeleteTransformerClick(transformer)
        }
    }

}