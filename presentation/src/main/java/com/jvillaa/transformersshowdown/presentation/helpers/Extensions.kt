package com.jvillaa.transformersshowdown.presentation.helpers

import android.util.Log
import com.jvillaa.transformersshowdown.domain.entity.DomainFight
import com.jvillaa.transformersshowdown.domain.entity.DomainBattleResults
import com.jvillaa.transformersshowdown.domain.entity.DomainFighters
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import java.util.*

/**
 * @author Juan Camilo Villa
 *
 * This function returns the possible fights given the fighters of
 * a battle
 *
 * @return Possible matches between fighters
 */
fun DomainFighters.getFights(): List<DomainFight> {
    val fights = mutableListOf<DomainFight>()
    try {
        autobots.forEachIndexed { index, autobot ->
            fights.add(DomainFight(autobot, decepticons[index]))
        }
    } catch (e: IndexOutOfBoundsException) {
        return fights
    }
    return fights
}

/**
 * @author Juan Camilo Villa
 *
 * Gets the list of unmatched transformers given the fighters of a battle
 *
 * @return List of transformers which are not going to fight
 */
fun DomainFighters.getUnmatchedTransformers(): List<DomainTransformer> {
    val unmatchedTransformers = mutableListOf<DomainTransformer>()
    val maxTransformersList = maxOf(autobots.size, decepticons.size)

    if (maxTransformersList == autobots.size) {
        for (x in (decepticons.size) until (maxTransformersList)) {
            unmatchedTransformers.add(autobots[x])
        }
    } else {
        for (x in (autobots.size) until (maxTransformersList)) {
            unmatchedTransformers.add(decepticons[x])
        }
    }

    return unmatchedTransformers
}

/**
 * @author Juan Camilo Villa
 *
 * Given a list of files, this function calculates the results of a battle, taking in count every
 * business rule previously defined
 *
 * @param unmatchedTransformers List of transformers which are not going to fight only to include them in the result
 * @throws IllegalStateException in case Predaking and Optimus fight each other
 */
fun List<DomainFight>.getFightsResult(unmatchedTransformers: List<DomainTransformer>): DomainBattleResults {

    val fightsNumber = this.size
    val winnerTransformers = mutableListOf<DomainTransformer>()
    var autobotsWinCount = 0
    var decepticonsWinCount = 0

    forEach {
        val autobot = it.autobot
        val decepticon = it.decepticon

        autobot.name = autobot.name.toLowerCase(Locale.getDefault())
        decepticon.name = decepticon.name.toLowerCase(Locale.getDefault())

        if ((autobot.name == "optimus prime" && decepticon.name == "predaking") || (decepticon.name == "optimus prime" && autobot.name == "predaking") || (autobot.name == "optimus prime" && decepticon.name == "optimus prime") || (autobot.name == "predaking" && decepticon.name == "predaking")) {
            throw IllegalStateException("Match end")
        } else {
            if (autobot.name == "optimus prime" || autobot.name == "predaking") {
                autobotsWinCount++
                winnerTransformers.add(autobot)
                return@forEach
            }
            if (decepticon.name == "optimus prime" || decepticon.name == "predaking") {
                decepticonsWinCount++
                winnerTransformers.add(decepticon)
                return@forEach
            }

            if ((autobot.courage - decepticon.courage >= 4) || (autobot.strength - decepticon.strength >= 3) || (autobot.skill - decepticon.skill >= 3)) {
                autobotsWinCount++
                winnerTransformers.add(autobot)
                return@forEach
            }
            if (decepticon.courage - autobot.courage >= 4 || (decepticon.strength - autobot.strength >= 3) || (decepticon.skill - autobot.skill >= 3)) {
                decepticonsWinCount++
                winnerTransformers.add(decepticon)
                return@forEach
            }

            when {
                autobot.getOverall() > decepticon.getOverall() -> {
                    autobotsWinCount++
                    winnerTransformers.add(autobot)
                }
                decepticon.getOverall() > autobot.getOverall() -> {
                    decepticonsWinCount++
                    winnerTransformers.add(decepticon)
                }
                else -> {
                    Log.v(this.javaClass.name, "Tie, no winner")
                }
            }
        }
    }

    return DomainBattleResults(
        fightsNumber = fightsNumber,
        winnerTeam = if (autobotsWinCount > decepticonsWinCount) "A" else if (decepticonsWinCount > autobotsWinCount) "D" else "T",
        winnerTransformers = winnerTransformers,
        survivors = unmatchedTransformers
    )
}