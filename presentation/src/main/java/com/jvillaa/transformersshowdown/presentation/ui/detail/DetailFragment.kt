package com.jvillaa.transformersshowdown.presentation.ui.detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutTransformerDetailBinding
import com.jvillaa.transformersshowdown.presentation.helpers.OnTransformerDetailInteractionListener
import com.jvillaa.transformersshowdown.presentation.ui.home.HomeActivity

/**
 * @author Juan Camilo Villa
 *
 * This fragment shows a transformer's detail for large devices (> sw600dp)
 */
class DetailFragment : Fragment(), OnTransformerDetailInteractionListener {

    private lateinit var binding: LayoutTransformerDetailBinding
    private lateinit var homeActivity: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutTransformerDetailBinding.bind(
            inflater.inflate(
                R.layout.layout_transformer_detail,
                container,
                false
            )
        )
        return binding.root
    }

    /**
     * Sets the interaction listener, to react to any user Transformers List click interaction
     *
     * @param context Fragment context
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = ((requireActivity()) as HomeActivity)
        homeActivity.setDetailInteractionListener(this)
    }

    /**
     * Sets and shows all the transformer info
     *
     * @param transformer Transformer to be detailed
     */
    override fun onShowTransformerDetail(transformer: DomainTransformer) {
        if (transformer.team == "A") {
            binding.teamLabel.text = getString(R.string.team_autobots)
            binding.teamBackgroundIcon.setImageResource(R.drawable.ic_autobot)
        } else {
            binding.teamLabel.text = getString(R.string.team_decepticons)
            binding.teamBackgroundIcon.setImageResource(R.drawable.ic_decepticon)
        }

        binding.transformerNameLabel.text = transformer.name

        binding.courageLabel.text =
            getString(R.string.courage_value_label, transformer.courage.toString())
        binding.speedLabel.text =
            getString(R.string.speed_value_label, transformer.speed.toString())
        binding.intelligenceLabel.text = getString(
            R.string.intelligence_value_label,
            transformer.intelligence.toString()
        )
        binding.strengthLabel.text =
            getString(R.string.strength_value_label, transformer.strength.toString())
        binding.enduranceLabel.text =
            getString(R.string.endurance_value_label, transformer.endurance.toString())
        binding.skillLabel.text =
            getString(R.string.skill_value_label, transformer.skill.toString())
        binding.rankLabel.text =
            getString(R.string.rank_value_label, transformer.rank.toString())
        binding.firepowerLabel.text =
            getString(R.string.firepower_value_label, transformer.firepower.toString())

        binding.editButton.setOnClickListener {
            homeActivity.editTransformer(transformer)
        }

        binding.deleteButton.setOnClickListener {
            homeActivity.deleteTransformer(transformer)
        }
    }

    /**
     * Static fragment creation
     */
    companion object {
        @JvmStatic
        fun newInstance() = DetailFragment()
    }
}