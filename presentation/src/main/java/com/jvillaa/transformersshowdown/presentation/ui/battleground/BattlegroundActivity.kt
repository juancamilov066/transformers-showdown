package com.jvillaa.transformersshowdown.presentation.ui.battleground

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.isVisible
import com.jvillaa.transformersshowdown.domain.entity.DomainFight
import com.jvillaa.transformersshowdown.domain.entity.DomainBattleResults
import com.jvillaa.transformersshowdown.domain.entity.DomainFighters
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.adapters.FightListAdapter
import com.jvillaa.transformersshowdown.presentation.adapters.TransformersListAdapter
import com.jvillaa.transformersshowdown.presentation.databinding.ActivityBattlegroundBinding
import com.jvillaa.transformersshowdown.presentation.helpers.Constants
import com.jvillaa.transformersshowdown.presentation.helpers.getFights
import com.jvillaa.transformersshowdown.presentation.helpers.getFightsResult
import com.jvillaa.transformersshowdown.presentation.helpers.getUnmatchedTransformers

/**
 * @author Juan Camilo Villa
 *
 * This activity holds the final encounter of the Autobots vs Decepticons
 * Here the user sees the fights that are going to happen and the results of the
 * battle
 */
class BattlegroundActivity : AppCompatActivity() {

    lateinit var binding: ActivityBattlegroundBinding
    private var fighters: DomainFighters? = null
    private lateinit var fights: List<DomainFight>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBattlegroundBinding.bind(
            View.inflate(
                this,
                R.layout.activity_battleground,
                null
            )
        )

        fighters = intent.getParcelableExtra(Constants.FIGHTERS_ARG)
        checkNotNull(fighters) { "Fighters can't be null" }

        setContentView(binding.root)
        observeAnimations()
    }

    /**
     * Sets up the views including lists
     */
    private fun setViews() {
        val fightsAdapter = FightListAdapter()
        binding.fightsList.apply {
            adapter = fightsAdapter
        }

        fights = fighters!!.getFights()
        fightsAdapter.setFights(fights)

        binding.seeResultsButton.setOnClickListener {
            setFightResults()
            binding.root.setTransition(R.id.endVersusFade, R.id.resultsFade)
            binding.root.transitionToEnd()
        }

        binding.finishButton.setOnClickListener {
            finish()
        }
    }

    /**
     * This function sets the match results to correspondent controls, also, handles
     * the case of a massive destruction
     */
    private fun setFightResults() {
        val result: DomainBattleResults
        try {
            result = fights.getFightsResult(fighters!!.getUnmatchedTransformers())
        } catch (e: IllegalStateException) {
            Toast.makeText(this, getString(R.string.match_ended_message), Toast.LENGTH_LONG)
                .show()
            finish()
            return
        }

        binding.totalFightsLabel.text =
            getString(R.string.total_fights_value_label, result.fightsNumber.toString())
        binding.winnerTeamLabel.text = getString(
            R.string.winner_team_value_label,
            if (result.winnerTeam == "A") "Autobots" else if (result.winnerTeam == "D") "Decepticons" else "Tie"
        )

        val winnersAdapter = TransformersListAdapter(this, null)
        binding.winnersList.apply {
            adapter = winnersAdapter
        }
        winnersAdapter.setTransformers(result.winnerTransformers)

        val survivorsAdapter = TransformersListAdapter(this, null)
        binding.survivorsList.apply {
            adapter = survivorsAdapter
        }
        survivorsAdapter.setTransformers(result.survivors)

        if (result.winnerTransformers.isEmpty()) {
            binding.winnersList.isVisible = false
            binding.noWinnersLabel.isVisible = true
        }
        if (result.survivors.isEmpty()) {
            binding.survivorsList.isVisible = false
            binding.noSurvivorsLabel.isVisible = true
        }
    }

    /**
     * This function starts observing the animations happening in this view
     * and reacts depending on the animation end
     */
    private fun observeAnimations() {
        binding.root.addTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
            }

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
            }

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p1 == R.id.endVersusScaleStartFade) {
                    binding.root.setTransition(R.id.endVersusScaleStartFade, R.id.endVersusFade)
                    binding.root.transitionToEnd()
                }
                if (p1 == R.id.endVersusFade) {
                    setViews()
                }
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
            }
        })
    }
}