package com.jvillaa.transformersshowdown.presentation.ui.manageTransformer

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @author Juan Camilo Villa
 *
 * This view model handles needed domain interactions
 *
 * @param transformersRepository used to edit and save transformers
 */
class ManageTransformerViewModel @ViewModelInject constructor(
    private val transformersRepository: TransformersRepository
) : ViewModel() {

    private val _saveTransformerLiveData = MediatorLiveData<Status<DomainTransformer>>()
    val saveTransformerLiveData = _saveTransformerLiveData

    private val _editTransformerLiveData = MediatorLiveData<Status<DomainTransformer>>()
    val editTransformerLiveData = _editTransformerLiveData

    /**
     * Saves a transformers and posts the result of the operation
     *
     * @param transformer Transformer to be created
     */
    fun saveTransformer(transformer: DomainTransformer) {
        _saveTransformerLiveData.postValue(Status.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = transformersRepository.saveTransformer(transformer)) {
                is Result.Success -> {
                    _saveTransformerLiveData.postValue(Status.Success(result.data))
                }
                is Result.Error -> {
                    _saveTransformerLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

    /**
     * Edits a transformers and posts the result of the operation
     *
     * @param transformer Transformer to be edites
     */
    fun editTransformer(transformer: DomainTransformer) {
        _editTransformerLiveData.postValue(Status.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = transformersRepository.editTransformer(transformer)) {
                is Result.Success -> {
                    _editTransformerLiveData.postValue(Status.Success(result.data))
                }
                is Result.Error -> {
                    _editTransformerLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

}