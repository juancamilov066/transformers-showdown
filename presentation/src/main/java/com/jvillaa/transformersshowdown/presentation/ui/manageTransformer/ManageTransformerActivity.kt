package com.jvillaa.transformersshowdown.presentation.ui.manageTransformer

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.core.widget.addTextChangedListener
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.ActivityManageTransformerBinding
import com.jvillaa.transformersshowdown.presentation.helpers.Constants
import com.jvillaa.transformersshowdown.presentation.helpers.ErrorDialogHelper
import com.jvillaa.transformersshowdown.presentation.helpers.IndeterminateDialogHelper
import com.jvillaa.transformersshowdown.presentation.helpers.OnSkillChangedListener
import com.jvillaa.transformersshowdown.presentation.ui.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Juan Camilo Villa
 *
 * This activity manages the transformer for their creation and edition
 */
@AndroidEntryPoint
class ManageTransformerActivity : BaseActivity(), OnSkillChangedListener {

    private val manageTransformerViewModel: ManageTransformerViewModel by viewModels()

    private var shouldUpdateList = false
    private var isEditing = false

    private lateinit var transformer: DomainTransformer
    private lateinit var indeterminateDialogHelper: IndeterminateDialogHelper
    private lateinit var errorDialogHelper: ErrorDialogHelper
    private lateinit var binding: ActivityManageTransformerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityManageTransformerBinding.bind(
            View.inflate(
                this,
                R.layout.activity_manage_transformer,
                null
            )
        )

        indeterminateDialogHelper = IndeterminateDialogHelper(this)
        errorDialogHelper = ErrorDialogHelper(this)

        setContentView(binding.root)
        setListeners()
        observeLiveData()
        initTransformer()
        setToolbar()
    }

    /**
     * Initializes the transformer to edit/create depending on the received params
     */
    private fun initTransformer() {
        val transformerToUpdate: DomainTransformer? =
            intent.getParcelableExtra(Constants.TRANSFORMER_TO_UPDATE_ARG)

        if (transformerToUpdate == null) {
            transformer = DomainTransformer(
                id = null,
                name = "",
                team = "A",
                strength = 1,
                intelligence = 1,
                speed = 1,
                endurance = 1,
                courage = 1,
                rank = 1,
                skill = 1,
                firepower = 1,
                teamIcon = null
            )
        } else {
            transformer = transformerToUpdate

            binding.courageSkillSelector.setCurrentValue(transformer.courage)
            binding.speedSkillSelector.setCurrentValue(transformer.speed)
            binding.strengthSkillSelector.setCurrentValue(transformer.strength)
            binding.skillSkillSelector.setCurrentValue(transformer.skill)
            binding.rankSkillSelector.setCurrentValue(transformer.rank)
            binding.intelligenceSkillSelector.setCurrentValue(transformer.intelligence)
            binding.enduranceSkillSelector.setCurrentValue(transformer.endurance)
            binding.firePowerSkillSelector.setCurrentValue(transformer.firepower)

            binding.transformerNameInput.setText(transformer.name)

            binding.teamSwitch.isChecked = transformer.team == "D"
            isEditing = true
        }
    }

    /**
     * Colors the selected team icon using the Material Switch
     */
    private fun setIconsColor(imageView: ImageView, colorId: Int) {
        ImageViewCompat.setImageTintList(
            imageView,
            ColorStateList.valueOf(ContextCompat.getColor(this, colorId))
        )
    }

    /**
     * Observes the manageTransformerViewModel live data objects
     */
    private fun observeLiveData() {
        val loadingDialog = indeterminateDialogHelper.getDialog()
        manageTransformerViewModel.saveTransformerLiveData.observe(this, {
            when (it) {
                is Status.Loading -> {
                    loadingDialog.show()
                }
                is Status.Success -> {
                    loadingDialog.dismiss()
                    showMessage(getString(R.string.transformer_created))
                    shouldUpdateList = true
                    reset()
                }
                is Status.Error -> {
                    loadingDialog.dismiss()
                    showMessage(getString(R.string.request_error))
                }
            }
        })

        manageTransformerViewModel.editTransformerLiveData.observe(this, {
            when (it) {
                is Status.Loading -> {
                    loadingDialog.show()
                }
                is Status.Success -> {
                    loadingDialog.dismiss()
                    showMessage(getString(R.string.transformer_edited))
                    shouldUpdateList = true
                    setUpdateResult()
                    reset()
                    finish()
                }
                is Status.Error -> {
                    loadingDialog.dismiss()
                    showMessage(getString(R.string.request_error))
                }
            }
        })
    }

    /**
     * Sets the toolbar info depending on the action the Activity is performing
     */
    private fun setToolbar() {
        setSupportActionBar(binding.toolbarIncludedLayout.baseToolbar)
        binding.toolbarIncludedLayout.toolbarTitle.text =
            if (isEditing) getString(
                R.string.edit_transformer_title
            ) else getString(
                R.string.create_transformer_title
            )
        supportActionBar?.let {
            it.setDisplayShowHomeEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }
    }

    /**
     * Sets the listeners to the interaction based views
     */
    private fun setListeners() {
        binding.teamSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                transformer.team = "D"
                setIconsColor(binding.autobotIcon, R.color.colorTeamUnselected)
                setIconsColor(binding.decepticonIcon, R.color.colorTeamSelected)
            } else {
                transformer.team = "A"
                setIconsColor(binding.decepticonIcon, R.color.colorTeamUnselected)
                setIconsColor(binding.autobotIcon, R.color.colorTeamSelected)
            }
        }

        binding.courageSkillSelector.setOnValueChangedListener(this)
        binding.speedSkillSelector.setOnValueChangedListener(this)
        binding.strengthSkillSelector.setOnValueChangedListener(this)
        binding.skillSkillSelector.setOnValueChangedListener(this)
        binding.rankSkillSelector.setOnValueChangedListener(this)
        binding.intelligenceSkillSelector.setOnValueChangedListener(this)
        binding.enduranceSkillSelector.setOnValueChangedListener(this)
        binding.firePowerSkillSelector.setOnValueChangedListener(this)

        binding.transformerNameInput.addTextChangedListener {
            transformer.name = it.toString()
        }

        binding.finishButton.setOnClickListener {
            if (transformer.name.isBlank()) {
                val dialog = errorDialogHelper.getDialog("Your Transformer needs a name")
                dialog.show()
            } else {
                if (isEditing) {
                    manageTransformerViewModel.editTransformer(transformer)
                } else {
                    manageTransformerViewModel.saveTransformer(transformer)
                }
            }
        }
    }

    /**
     * Resets the transformer object when the previous one was created successfully, to allow the
     * creation of 2 or more transformers
     *
     * Also resets the controls
     */
    private fun reset() {
        transformer = DomainTransformer(
            id = null,
            name = "",
            team = "A",
            strength = 1,
            intelligence = 1,
            speed = 1,
            endurance = 1,
            courage = 1,
            rank = 1,
            skill = 1,
            firepower = 1,
            teamIcon = null
        )

        binding.courageSkillSelector.reset()
        binding.speedSkillSelector.reset()
        binding.strengthSkillSelector.reset()
        binding.skillSkillSelector.reset()
        binding.rankSkillSelector.reset()
        binding.intelligenceSkillSelector.reset()
        binding.enduranceSkillSelector.reset()
        binding.firePowerSkillSelector.reset()

        binding.transformerNameInput.setText(String())
        binding.teamSwitch.isChecked = false
    }

    /**
     * Sets the activity result value to know if we need to update the list or not
     */
    private fun setUpdateResult() {
        val intent = Intent()
        intent.putExtra(Constants.SHOULD_UPDATE_ARG, shouldUpdateList)
        setResult(Constants.UPDATE_ACTIVITY_CODE, intent)
    }

    /**
     * Handles the skill selectors change listeners to update the Transformer Object
     */
    override fun onSkillValueChanged(view: View, previousValue: Int, currentValue: Int) {
        when (view.id) {
            R.id.courageSkillSelector -> {
                transformer.courage = currentValue
            }
            R.id.speedSkillSelector -> {
                transformer.speed = currentValue
            }
            R.id.strengthSkillSelector -> {
                transformer.strength = currentValue
            }
            R.id.firePowerSkillSelector -> {
                transformer.firepower = currentValue
            }
            R.id.enduranceSkillSelector -> {
                transformer.endurance = currentValue
            }
            R.id.rankSkillSelector -> {
                transformer.rank = currentValue
            }
            R.id.skillSkillSelector -> {
                transformer.skill = currentValue
            }
            R.id.intelligenceSkillSelector -> {
                transformer.intelligence = currentValue
            }
        }
    }

    /**
     * Sets the activity result before leaving
     */
    override fun onBackPressed() {
        setUpdateResult()
        super.onBackPressed()
    }

    /**
     * Sets the activity result before leaving
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setUpdateResult()
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}