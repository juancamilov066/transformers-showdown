package com.jvillaa.transformersshowdown.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jvillaa.transformersshowdown.domain.entity.DomainFight
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutItemFightersBinding

/**
 * @author Juan Camilo Villa
 *
 * This adapter is used to display the fight contenders
 */
class FightListAdapter : RecyclerView.Adapter<FightListAdapter.FightViewHolder>() {

    private val items = mutableListOf<DomainFight>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FightViewHolder {
        val view =
            LayoutItemFightersBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FightViewHolder(view)
    }

    override fun onBindViewHolder(holder: FightViewHolder, position: Int) {
        val fight = items[position]
        holder.bind(fight)
    }

    /**
     * This function set the items that we're going to
     * display, after doing a list clean up
     *
     * @param fights The fights that the Battle is going to have
     */
    fun setFights(fights: List<DomainFight>) {
        items.clear()
        items.addAll(fights)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    inner class FightViewHolder(val binding: LayoutItemFightersBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(fight: DomainFight) {
            binding.autobotNameLabel.text = fight.autobot.name
            binding.decepticonNameLabel.text = fight.decepticon.name
        }
    }
}