package com.jvillaa.transformersshowdown.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutItemTransformerBinding
import com.jvillaa.transformersshowdown.presentation.helpers.OnTransformerClickListener

/**
 * @author Juan Camilo Villa
 *
 * This adapter is used to display a list of transformers
 *
 * @param context Used to get string resources
 * @param transformerClickListener Interface to handle the user click interaction with each item
 */
class TransformersListAdapter(
    private val context: Context,
    private val transformerClickListener: OnTransformerClickListener?
) : RecyclerView.Adapter<TransformersListAdapter.TransformersListViewHolder>() {

    private val items = mutableListOf<DomainTransformer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransformersListViewHolder {
        val view =
            LayoutItemTransformerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransformersListViewHolder(view)
    }

    override fun onBindViewHolder(holder: TransformersListViewHolder, position: Int) {
        val transformer = items[position]
        holder.bind(transformer)
    }

    override fun getItemCount(): Int = items.size

    /**
     * This function set the items that we're going to
     * display, after doing a list clean up
     *
     * @param transformers The transformers we're going to list
     */
    fun setTransformers(transformers: List<DomainTransformer>) {
        items.clear()
        items.addAll(transformers)
        notifyDataSetChanged()
    }

    inner class TransformersListViewHolder(private val binding: LayoutItemTransformerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(transformer: DomainTransformer) {
            if (transformerClickListener != null) {
                binding.root.setOnClickListener {
                    transformerClickListener.onClick(transformer)
                }
            }

            if (transformer.team == "A") {
                binding.transformerTeamIcon.setImageResource(R.drawable.ic_autobot)
            } else {
                binding.transformerTeamIcon.setImageResource(R.drawable.ic_decepticon)
            }

            binding.transformerNameLabel.text = transformer.name

            binding.courageLabel.text = transformer.courage.toString()
            binding.enduranceLabel.text = transformer.endurance.toString()
            binding.firepowerLabel.text = transformer.firepower.toString()
            binding.intelligenceLabel.text = transformer.intelligence.toString()
            binding.speedLabel.text = transformer.speed.toString()
            binding.strengthLabel.text = transformer.strength.toString()
            binding.overallLabel.text = context.getString(
                R.string.overall_value_label,
                transformer.getOverall().toString()
            )

            binding.skillLabel.text =
                context.getString(R.string.skill_value_label, transformer.skill.toString())
            binding.rankLabel.text =
                context.getString(R.string.rank_value_label, transformer.rank.toString())
        }
    }

}