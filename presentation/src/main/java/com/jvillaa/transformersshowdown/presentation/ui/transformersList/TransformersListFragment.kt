package com.jvillaa.transformersshowdown.presentation.ui.transformersList

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.adapters.TransformersListAdapter
import com.jvillaa.transformersshowdown.presentation.databinding.FragmentTransformersListBinding
import com.jvillaa.transformersshowdown.presentation.helpers.*
import com.jvillaa.transformersshowdown.presentation.ui.battleground.BattlegroundActivity
import com.jvillaa.transformersshowdown.presentation.ui.home.HomeActivity
import com.jvillaa.transformersshowdown.presentation.ui.manageTransformer.ManageTransformerActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
/**
 * @author Juan Camilo Villa
 *
 * This activity holds the transformers list
 */
class TransformersListFragment : Fragment(), OnTransformerInteractionListener,
    OnTransformerClickListener {

    private val transformersListViewModel: TransformersListViewModel by viewModels()
    private lateinit var transformersListAdapter: TransformersListAdapter
    private lateinit var errorDialogHelper: ErrorDialogHelper
    private lateinit var binding: FragmentTransformersListBinding
    private lateinit var homeActivity: HomeActivity
    private lateinit var transformers: List<DomainTransformer>

    private val indeterminateDialogHelper by lazy {
        IndeterminateDialogHelper(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTransformersListBinding.bind(
            inflater.inflate(
                R.layout.fragment_transformers_list,
                container,
                false
            )
        )
        return binding.root
    }

    /**
     * Sets the edition listener to be notified when to update the list
     *
     * @param context Fragment context
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = ((requireActivity()) as HomeActivity)
        homeActivity.setTransformersUpdateListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        errorDialogHelper = ErrorDialogHelper(requireContext())

        transformersListAdapter = TransformersListAdapter(
            requireContext(),
            this
        )

        binding.createTransformerFab.setOnClickListener {
            startActivityForResult(
                Intent(requireActivity(), ManageTransformerActivity::class.java),
                Constants.UPDATE_ACTIVITY_CODE
            )
        }

        binding.startFightButton.setOnClickListener {
            transformersListViewModel.startFight(transformers)
        }

        binding.transformersRecyclerView.apply {
            adapter = transformersListAdapter
        }

        observeLiveData()
        transformersListViewModel.fetchTransformers()
    }

    /**
     * Observes the transformers list view model live data objects
     */
    private fun observeLiveData() {
        val dialog = indeterminateDialogHelper.getDialog()
        transformersListViewModel.transformersListLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Status.Loading -> {
                    dialog.show()
                }
                is Status.Success -> {
                    dialog.dismiss()
                    transformers = it.data
                    if (transformers.isEmpty()) {
                        toggleUIContent(true)
                    } else {
                        toggleUIContent(false)
                        transformersListAdapter.setTransformers(transformers)
                    }
                }
                is Status.Error -> {
                    transformers = listOf()
                    dialog.dismiss()
                    homeActivity.showMessage(getString(R.string.request_error))
                }
            }
        })

        transformersListViewModel.fightLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Success -> {
                    val intent = Intent(requireContext(), BattlegroundActivity::class.java)
                    intent.putExtra(Constants.FIGHTERS_ARG, it.data)
                    startActivity(intent)
                }
                is Result.Error -> {
                    val message = it.error.message!!
                    errorDialogHelper.getDialog(message).show()
                }
            }
        })
    }

    /**
     * This function switch the content depending on the list size
     */
    private fun toggleUIContent(isListEmpty: Boolean) {
        binding.errorContainer.isVisible = isListEmpty
        binding.transformersListContainer.isVisible = !isListEmpty
    }

    /**
     * Function that detects list item clicks
     */
    override fun onClick(transformer: DomainTransformer) {
        homeActivity.onTransformerSelected(transformer)
    }

    /**
     * Function to update transformers list
     */
    override fun updateTransformers() {
        transformersListViewModel.fetchTransformers()
    }

    /**
     * Static fragment creation
     */
    companion object {
        @JvmStatic
        fun newInstance() = TransformersListFragment()
    }
}