package com.jvillaa.transformersshowdown.presentation.ui.transformersList

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jvillaa.transformersshowdown.domain.entity.DomainFighters
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransformersListViewModel @ViewModelInject constructor(
    private val transformersRepository: TransformersRepository
) : ViewModel() {

    private val _transformersListLiveData = MediatorLiveData<Status<List<DomainTransformer>>>()
    val transformersListLiveData = _transformersListLiveData

    private val _fightLiveData = MediatorLiveData<Result<DomainFighters>>()
    val fightLiveData = _fightLiveData

    fun startFight(transformers: List<DomainTransformer>) {
        if (transformers.size < 2) {
            _fightLiveData.postValue(Result.Error(Exception("You don't have enough Transformers to start a fight")))
        } else {
            val autobots = transformers.filter {
                it.team == "A"
            }
            val decepticons = transformers.filter {
                it.team == "D"
            }

            if (autobots.isNotEmpty() && decepticons.isNotEmpty()) {
                val fighters = DomainFighters(
                    autobots = autobots.sortedByDescending { it.rank },
                    decepticons = decepticons.sortedByDescending { it.rank }
                )
                _fightLiveData.postValue(Result.Success(fighters))
            } else {
                _fightLiveData.postValue(Result.Error(Exception("You need to have at least one Transformer from each faction to fight")))
            }
        }
    }

    fun fetchTransformers() {
        _transformersListLiveData.postValue(Status.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = transformersRepository.getTransformers()) {
                is Result.Success -> {
                    _transformersListLiveData.postValue(Status.Success(result.data))
                }
                is Result.Error -> {
                    _transformersListLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

}