package com.jvillaa.transformersshowdown.presentation.ui

import android.content.SharedPreferences
import android.content.res.Configuration
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * Basic activity which contains common usages
 */
open class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    /**
     * Displays a toast with given message
     *
     * @param message Toast message
     */
    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}