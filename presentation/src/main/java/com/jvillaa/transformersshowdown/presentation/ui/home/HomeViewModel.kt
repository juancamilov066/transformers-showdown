package com.jvillaa.transformersshowdown.presentation.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @author Juan Camilo Villa
 *
 * This view model handles needed domain interactions
 *
 * @param transformersRepository Used to perform data operation
 */
class HomeViewModel @ViewModelInject constructor(
    private val transformersRepository: TransformersRepository
) : ViewModel() {

    private val _deleteTransformerLiveData = MediatorLiveData<Status<Unit>>()
    val deleteTransformersLiveData = _deleteTransformerLiveData

    /**
     * This function triggers a Transformer deletion by their id and then
     * posts the result
     *
     * @param id Id of the transformer to delete
     */
    fun deleteTransformerById(id: String) {
        _deleteTransformerLiveData.postValue(Status.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = transformersRepository.deleteTransformerById(id)) {
                is Result.Success -> {
                    _deleteTransformerLiveData.postValue(Status.Success(Unit))
                }
                is Result.Error -> {
                    _deleteTransformerLiveData.postValue(Status.Error(result.error))
                }
            }
        }
    }

}