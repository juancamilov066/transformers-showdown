package com.jvillaa.transformersshowdown.presentation.helpers

/**
 * @author Juan Camilo Villa
 *
 * Constants to be used in the presentation side
 */
object Constants {
    const val UPDATE_ACTIVITY_CODE = 1

    const val TRANSFORMER_TO_UPDATE_ARG = "TRANSFORMER_TO_UPDATE_ARG"
    const val SHOULD_UPDATE_ARG = "SHOULD_UPDATE_ARG"
    const val FIGHTERS_ARG = "FIGHTERS_ARG"
}