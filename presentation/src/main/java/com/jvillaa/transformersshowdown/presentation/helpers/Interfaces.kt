package com.jvillaa.transformersshowdown.presentation.helpers

import android.view.View
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer

/**
 * Interface used to observe the value of any Skill Selector Custom View
 */
interface OnSkillChangedListener {
    fun onSkillValueChanged(view: View, previousValue: Int, currentValue: Int)
}

/**
 * Interface to communicate HomeActivity with any Fragment
 * it fires this method when any transformers list needs to be updated
 */
interface OnTransformerInteractionListener {
    fun updateTransformers()
}

/**
 * Interface to communicate HomeActivity with any Fragment
 * It fires this method when a transformer was selected and needs to be presented
 * Used in tablets
 */
interface OnTransformerDetailInteractionListener {
    fun onShowTransformerDetail(transformer: DomainTransformer)
}

/**
 * Interface that reacts to click user interaction with a transformer
 */
interface OnTransformerClickListener {
    fun onClick(transformer: DomainTransformer)
}

/**
 * Interface to react to any edition or deletion event fired from Detail Dialog or Transformer Side View
 */
interface OnTransformerEditionListener {
    fun onDeleteTransformerClick(transformer: DomainTransformer)
    fun onEditTransformerClick(transformer: DomainTransformer)
}