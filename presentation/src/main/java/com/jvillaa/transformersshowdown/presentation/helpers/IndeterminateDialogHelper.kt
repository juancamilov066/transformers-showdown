package com.jvillaa.transformersshowdown.presentation.helpers

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutDialogLoadingIndeterminateBinding

/**
 * @author Juan Camilo Villa
 *
 * This dialog helper simplifies the IndeterminateDialogHelper building process
 *
 * @param context used to inflate the custom view
 */
class IndeterminateDialogHelper(private val context: Context) {

    private val binding = LayoutDialogLoadingIndeterminateBinding.bind(
        View.inflate(
            context,
            R.layout.layout_dialog_loading_indeterminate,
            null
        )
    )

    /**
     * This function randomizes the loading messages to pick one at the end
     *
     * @return Funny randomized message
     */
    private fun getRandomizedMessage(): String {
        loadingMessages.shuffle()
        return loadingMessages[0]
    }

    /**
     * This function builds the dialog
     *
     * @return Built dialog
     */
    fun getDialog(): AlertDialog {
        binding.dialogVariableMessage.text = getRandomizedMessage()

        val dialog = AlertDialog
            .Builder(context)
            .setView(binding.root)
            .setCancelable(false)
            .create()

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return dialog
    }

    companion object {
        val loadingMessages = arrayOf(
            "Waxing Optimus tires",
            "Preparing Megatron's tea",
            "Changing Bumblebee's voice cassette",
            "Cleaning Ironhide's weapons"
        )
    }

}