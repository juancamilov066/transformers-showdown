package com.jvillaa.transformersshowdown.presentation.helpers

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutDialogErrorBinding

/**
 * @author Juan Camilo Villa
 *
 * This dialog helper simplifies the ErrorDialogHelper building process
 *
 * @param context used to inflate the custom view
 */
class ErrorDialogHelper(private val context: Context) {

    /**
     * This function builds the dialog
     *
     * @return Built dialog
     */
    fun getDialog(error: String): AlertDialog {
        val binding = LayoutDialogErrorBinding.bind(
            View.inflate(
                context,
                R.layout.layout_dialog_error,
                null
            )
        )

        val dialog = AlertDialog
            .Builder(context)
            .setView(binding.root)
            .setCancelable(false)
            .create()

        binding.acceptButton.setOnClickListener {
            dialog.dismiss()
        }

        binding.dialogVariableMessage.text = error
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return dialog
    }

}