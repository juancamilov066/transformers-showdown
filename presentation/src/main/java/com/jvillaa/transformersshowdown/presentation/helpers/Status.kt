package com.jvillaa.transformersshowdown.presentation.helpers

/**
 * @author Juan Camilo Villa
 *
 * This sealed class contains all the possible states of the data we handle in the view side
 */
sealed class Status<out T> {
    object Loading: Status<Nothing>()
    data class Success<out T>(val data: T): Status<T>()
    data class Error(val error: Exception): Status<Nothing>()
}