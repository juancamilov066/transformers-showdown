package com.jvillaa.transformersshowdown.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.constraintlayout.motion.widget.MotionLayout
import com.jvillaa.transformersshowdown.domain.helpers.Constants
import com.jvillaa.transformersshowdown.presentation.helpers.Status
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.ActivitySplashBinding
import com.jvillaa.transformersshowdown.presentation.helpers.IndeterminateDialogHelper
import com.jvillaa.transformersshowdown.presentation.ui.BaseActivity
import com.jvillaa.transformersshowdown.presentation.ui.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Juan Camilo Villa
 *
 * This Activity is the app entry point, displays an animation
 * and handles the auth operations
 */
@AndroidEntryPoint
class SplashActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashBinding
    private val indeterminateDialogHelper by lazy {
        IndeterminateDialogHelper(this)
    }
    private val splashViewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            ActivitySplashBinding.bind(View.inflate(this, R.layout.activity_splash, null))
        setContentView(binding.root)

        setViews()
        observeAnimations()
        observeLiveData()
    }

    /**
     * Sets up the views
     */
    private fun setViews() {
        binding.startButton.setOnClickListener {
            val needsLogin = sharedPreferences.getString(Constants.PREF_TOKEN, "").isNullOrBlank()
            if (needsLogin) {
                splashViewModel.login()
            } else {
                runStartAnimation()
            }
        }
    }

    /**
     * Observes the splash view model's live data objects
     */
    private fun observeLiveData() {
        val dialog = indeterminateDialogHelper.getDialog()
        splashViewModel.authTokenLiveData.observe(this, {
            when (it) {
                is Status.Loading -> {
                    dialog.show()
                }
                is Status.Success -> {
                    splashViewModel.saveToken(it.data)
                    dialog.dismiss()
                }
                is Status.Error -> {
                    dialog.dismiss()
                    showMessage(getString(R.string.request_error))
                }
            }
        })

        splashViewModel.tokenStorageLiveData.observe(this, {
            when (it) {
                is Status.Success -> {
                    dialog.dismiss()
                    runStartAnimation()
                }
                is Status.Error -> {
                    dialog.dismiss()
                }
                else -> {
                    return@observe
                }
            }
        })
    }

    /**
     * Starts the icon magnification animation
     */
    private fun runStartAnimation() {
        binding.splashBaseMotionLayout.setTransition(
            R.id.mainAnimationEnd,
            R.id.iconMagnificationEnd
        )
        binding.splashBaseMotionLayout.transitionToEnd()
    }

    /**
     * Observes the animations to react to them when finished
     */
    private fun observeAnimations() {
        binding.splashBaseMotionLayout.addTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
            }

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
            }

            override fun onTransitionCompleted(p0: MotionLayout?, constraintId: Int) {
                if (constraintId == R.id.iconMagnificationEnd) {
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    finish()
                }
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
            }
        })
    }

}