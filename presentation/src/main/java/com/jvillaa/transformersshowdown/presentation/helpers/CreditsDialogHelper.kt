package com.jvillaa.transformersshowdown.presentation.helpers

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.jvillaa.transformersshowdown.presentation.R
import com.jvillaa.transformersshowdown.presentation.databinding.LayoutDialogCreditsBinding

/**
 * @author Juan Camilo Villa
 *
 * This dialog helper simplifies the CreditsDialogHelper building process
 *
 * @param context used to inflate the custom view
 */
class CreditsDialogHelper(private val context: Context) {

    /**
     * This function builds the dialog
     *
     * @return Built dialog
     */
    fun getDialog(): AlertDialog {
        val binding = LayoutDialogCreditsBinding.bind(
            View.inflate(
                context,
                R.layout.layout_dialog_credits,
                null
            )
        )

        val dialog = AlertDialog
            .Builder(context)
            .setView(binding.root)
            .setCancelable(false)
            .create()

        binding.acceptButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return dialog
    }

}