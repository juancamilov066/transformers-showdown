# TRANSFORMERS: Showdown

Is a fun app where you can manage your own Transformers and then set them up into a exciting battle. Make your faction win in this amazing app.

## Installation

Install provided APK into your device, no special permission is required

## Usage

- Open the application and click on Start Button
- Initially you're not going to have any transformer, click + button to create a Transformer
- Add a name, choose a faction and set up the skills. After that, click Finish button
- Repeat that process as much as you want, the more transformers you have, the more exciting the fight will be
- Click on Fight! button
- See the upcoming fights, and click See Results button
- Check if your favorite team won, and if your favorite Transformers survived

## License
[MIT](https://choosealicense.com/licenses/mit/)