package com.jvillaa.transformersshowdown.data.repository.transformer

import com.jvillaa.transformersshowdown.data.datasource.remote.transformers.TransformersRemoteDataSource
import com.jvillaa.transformersshowdown.data.mappers.Mapper
import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.data.repository.TransformersRepositoryImpl
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransformerRepositoryTest {

    //SUT
    private lateinit var transformersRepositoryImpl: TransformersRepositoryImpl

    //Collaborators
    private val transformersRemoteDataSource: TransformersRemoteDataSource = mockk()
    private lateinit var transformersObjectMapper: Mapper<ApiTransformer, DomainTransformer>

    @BeforeEach
    fun init() {
        transformersObjectMapper = mockk {
            every { mapApiToDomain(any()) } returns mockk(relaxed = true)
            every { mapDomainToApi(any()) } returns mockk(relaxed = true)
        }
        transformersRepositoryImpl =
            TransformersRepositoryImpl(transformersRemoteDataSource, transformersObjectMapper)
    }

    @AfterEach
    fun clearMocks() {
        clearMocks(transformersRemoteDataSource, transformersObjectMapper)
    }

    @Nested
    @DisplayName("Given transformersRepositoryImpl is called")
    inner class TransformersRepositoryTests {
        @Test
        @DisplayName("When it tries to list transformers, remote data source should list data")
        fun getTransformers() {
            coEvery {
                transformersRemoteDataSource.getTransformers()
            } returns Result.Success(mockk(relaxed = true))
            runBlocking { transformersRepositoryImpl.getTransformers() }
            coVerify { transformersRemoteDataSource.getTransformers() }
        }

        @Test
        @DisplayName("When it tries to edit a transformer, remote data source should edit transformer")
        fun editTransformers() {
            val domainTransformer: DomainTransformer = mockk(relaxed = true)
            coEvery { transformersRemoteDataSource.editTransformer(any()) } returns Result.Success(
                mockk(relaxed = true)
            )
            runBlocking { transformersRepositoryImpl.editTransformer(domainTransformer) }
            coVerify { transformersRemoteDataSource.editTransformer(any()) }
        }
        @Test
        @DisplayName("When it tries to delete a transformer, remote data source should delete transformer")
        fun deleteTransformers() {
            val id = "1234ABCD"
            coEvery { transformersRemoteDataSource.deleteTransformerById(id) } returns Result.Success(
                mockk(relaxed = true)
            )
            runBlocking { transformersRepositoryImpl.deleteTransformerById(id) }
            coVerify { transformersRemoteDataSource.deleteTransformerById(id) }
        }
    }

}