package com.jvillaa.transformersshowdown.data.repository.auth

import com.jvillaa.transformersshowdown.data.datasource.local.auth.AuthLocalDataSource
import com.jvillaa.transformersshowdown.data.datasource.remote.auth.AuthRemoteDataSource
import com.jvillaa.transformersshowdown.data.repository.AuthRepositoryImpl
import com.jvillaa.transformersshowdown.domain.helpers.Result
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuthRepositoryTest {

    //SUT
    private lateinit var authRepository: AuthRepositoryImpl

    //Mockk
    private val authRemoteDataSource: AuthRemoteDataSource = mockk()
    private val authLocalDataSource: AuthLocalDataSource = mockk()

    @BeforeEach
    fun init() {
        clearMocks(authLocalDataSource, authRemoteDataSource)
        authRepository = AuthRepositoryImpl(authRemoteDataSource, authLocalDataSource)
    }

    @Nested
    @DisplayName("Given authRepositoryImpl is called")
    inner class AuthRepositoryTests {
        @Test
        @DisplayName("When it tries to authenticate, the remote data source should authenticate the user")
        fun retrieveToken() {
            coEvery {
                authRemoteDataSource.retrieveAuthToken()
            } returns Result.Success("ABCD1234")
            runBlocking { authRepository.retrieveAuthToken() }
            coVerify { authRemoteDataSource.retrieveAuthToken() }
        }

        @Test
        @DisplayName("When user has a token, the local data source should save it")
        fun saveToken() {
            val token = "ABCD1234"
            coEvery {
                authLocalDataSource.saveAuthToken(token)
            } returns Result.Success(mockk(relaxed = true))
            runBlocking { authRepository.saveAuthToken(token) }
            coVerify { authLocalDataSource.saveAuthToken(token) }
        }
    }

}