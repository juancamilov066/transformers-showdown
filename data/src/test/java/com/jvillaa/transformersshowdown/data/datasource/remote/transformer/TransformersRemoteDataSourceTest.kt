package com.jvillaa.transformersshowdown.data.datasource.remote.transformer

import com.jvillaa.transformersshowdown.data.api.TransformerApi
import com.jvillaa.transformersshowdown.data.datasource.remote.transformers.TransformersRemoteDataSourceImpl
import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import retrofit2.Response

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransformersRemoteDataSourceTest {

    //SUT
    private lateinit var transformersRemoteDataSourceImpl: TransformersRemoteDataSourceImpl

    //Collaborators
    private val transformerApi: TransformerApi = mockk()

    @BeforeEach
    fun init() {
        clearMocks(transformerApi)
        transformersRemoteDataSourceImpl = TransformersRemoteDataSourceImpl(transformerApi)
    }

    @Nested
    @DisplayName("Given transformersRemoteDataSourceImpl is called")
    inner class TransformersRepositoryTests {
        @Test
        @DisplayName("When it tries to list transformers, transformersApi should list data")
        fun getTransformers() {
            coEvery {
                transformerApi.getCreatedTransformers()
            } returns Response.success(mockk(relaxed = true))
            runBlocking { transformersRemoteDataSourceImpl.getTransformers() }
            coVerify { transformerApi.getCreatedTransformers() }
        }

        @Test
        @DisplayName("When it tries to edit a transformer, transformersApi should edit transformer")
        fun editTransformers() {
            val apiTransformer: ApiTransformer = mockk(relaxed = true)
            coEvery { transformerApi.updateTransformer(any()) } returns Response.success(mockk(relaxed = true))
            runBlocking { transformersRemoteDataSourceImpl.editTransformer(apiTransformer) }
            coVerify { transformerApi.updateTransformer(any()) }
        }
        @Test
        @DisplayName("When it tries to delete a transformer, transformersApi should delete transformer")
        fun deleteTransformers() {
            val id = "1234ABCD"
            coEvery { transformerApi.deleteTransformersById(id) } returns Response.success(mockk(relaxed = true))
            runBlocking { transformersRemoteDataSourceImpl.deleteTransformerById(id) }
            coVerify { transformerApi.deleteTransformersById(id) }
        }
    }

}