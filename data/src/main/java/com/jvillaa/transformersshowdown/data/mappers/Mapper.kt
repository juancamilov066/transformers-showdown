package com.jvillaa.transformersshowdown.data.mappers

/**
 * @author Juan Camilo Villa
 *
 * Interface to standardize how to convert objects between layers
 */
interface Mapper<API, DOMAIN> {
    fun mapApiToDomain(param: API): DOMAIN
    fun mapDomainToApi(param: DOMAIN): API
}