package com.jvillaa.transformersshowdown.data.datasource.local.auth

import com.jvillaa.transformersshowdown.domain.helpers.Result

/**
 * @author Juan Camilo Villa
 *
 * This interface is the contract for all the possible local data operations
 * related with Auth
 */
interface AuthLocalDataSource {
    /**
     * This function saves a previously acquired token
     *
     * @param token Auth token
     * @return Result of the save operation
     */
    suspend fun saveAuthToken(token: String): Result<Unit>
}