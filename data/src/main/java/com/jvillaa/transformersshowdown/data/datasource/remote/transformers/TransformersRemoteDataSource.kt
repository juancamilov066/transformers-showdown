package com.jvillaa.transformersshowdown.data.datasource.remote.transformers

import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.data.model.ApiTransformers
import com.jvillaa.transformersshowdown.domain.helpers.Result

/**
 * @author Juan Camilo Villa
 *
 * This interface is the contract for all the transformers remote operations
 */
interface TransformersRemoteDataSource {
    /**
     * This function gets all the created transformers remotely
     *
     * @return Result with transformers list
     */
    suspend fun getTransformers(): Result<ApiTransformers>

    /**
     * This function saves a transformer remotely
     *
     * @param apiTransformer transformer to create
     * @return Result with previously created transformer
     */
    suspend fun saveTransformer(apiTransformer: ApiTransformer): Result<ApiTransformer>

    /**
     * This function deletes a transformer remotely
     *
     * @param id id of the transformer to be deleted
     * @return Success or error in operation
     */
    suspend fun deleteTransformerById(id: String): Result<Unit>

    /**
     * This function edits a transformer remotely
     *
     * @param apiTransformer transformer to be edited
     * @return Result with edited transformer
     */
    suspend fun editTransformer(apiTransformer: ApiTransformer): Result<ApiTransformer>
}