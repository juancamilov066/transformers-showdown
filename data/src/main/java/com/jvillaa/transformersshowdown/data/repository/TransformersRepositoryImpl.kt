package com.jvillaa.transformersshowdown.data.repository

import com.jvillaa.transformersshowdown.data.datasource.remote.transformers.TransformersRemoteDataSource
import com.jvillaa.transformersshowdown.data.mappers.Mapper
import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.domain.repository.TransformersRepository
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This class describes how to do any transformer related operation
 * In this case, only manages remote data flows since we don't do any
 * local operation with Transformers data
 *
 * @param transformersObjectMapper mapper to convert entities
 * @param transformersRemoteDataSource remote data source
 */
class TransformersRepositoryImpl @Inject constructor(
    private val transformersRemoteDataSource: TransformersRemoteDataSource,
    private val transformersObjectMapper: Mapper<ApiTransformer, DomainTransformer>
) : TransformersRepository {

    /**
     * This function describes where to get the transformers list and also
     * converts it to a Domain readable result
     *
     * @return Domain transformers list
     */
    override suspend fun getTransformers(): Result<List<DomainTransformer>> {
        return when (val result = transformersRemoteDataSource.getTransformers()) {
            is Result.Success -> {
                Result.Success(apiTransformersListToDomain(result.data.transformers))
            }
            is Result.Error -> {
                Result.Error(result.error)
            }
        }
    }

    /**
     * This function describes where to save a transformer, then converts the result to
     * a domain readable result
     *
     * @param domainTransformer Transformer to be created
     * @return Created domain transformer
     */
    override suspend fun saveTransformer(domainTransformer: DomainTransformer): Result<DomainTransformer> {
        return when (val result = transformersRemoteDataSource.saveTransformer(
            transformersObjectMapper.mapDomainToApi(domainTransformer)
        )) {
            is Result.Success -> {
                Result.Success(transformersObjectMapper.mapApiToDomain(result.data))
            }
            is Result.Error -> {
                Result.Error(result.error)
            }
        }
    }

    /**
     * This function describes where to delete a transformer
     *
     * @param id id of the transformer to be deleted
     * @return Success or error in the operation
     */
    override suspend fun deleteTransformerById(id: String): Result<Unit> {
        return transformersRemoteDataSource.deleteTransformerById(id)
    }

    /**
     * This function describes where to edit a transformer
     *
     * @param domainTransformer transformer to be edited
     * @return Edited transformer
     */
    override suspend fun editTransformer(domainTransformer: DomainTransformer): Result<DomainTransformer> {
        return when (val result = transformersRemoteDataSource.editTransformer(
            transformersObjectMapper.mapDomainToApi(domainTransformer)
        )) {
            is Result.Success -> {
                Result.Success(transformersObjectMapper.mapApiToDomain(result.data))
            }
            is Result.Error -> {
                Result.Error(result.error)
            }
        }
    }

    /**
     * This function maps a list of api entities into a domain list
     *
     * @param apiList Api entities list
     * @return Domain entities list
     */
    private fun apiTransformersListToDomain(apiList: List<ApiTransformer>): List<DomainTransformer> {
        return apiList.map {
            transformersObjectMapper.mapApiToDomain(it)
        }
    }
}