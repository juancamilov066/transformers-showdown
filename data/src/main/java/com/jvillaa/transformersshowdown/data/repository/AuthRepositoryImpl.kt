package com.jvillaa.transformersshowdown.data.repository

import com.jvillaa.transformersshowdown.data.datasource.local.auth.AuthLocalDataSource
import com.jvillaa.transformersshowdown.data.datasource.remote.auth.AuthRemoteDataSource
import com.jvillaa.transformersshowdown.domain.helpers.Result
import com.jvillaa.transformersshowdown.domain.repository.AuthRepository
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This class describes how to do any auth related operation
 * and also defines how to manage remote and local data flows
 *
 * @param authLocalDataSource Local data source
 * @param authRemoteDataSource Remote data source
 */
class AuthRepositoryImpl @Inject constructor(
    private val authRemoteDataSource: AuthRemoteDataSource,
    private val authLocalDataSource: AuthLocalDataSource
) : AuthRepository {

    /**
     * This function describes where to retrieve the token
     *
     * @return Result with the auth token
     */
    override suspend fun retrieveAuthToken(): Result<String> {
        return authRemoteDataSource.retrieveAuthToken()
    }

    /**
     * This function describes where to save the auth token
     *
     * @param token Auth token
     * @return Success or error in the operation
     */
    override suspend fun saveAuthToken(token: String): Result<Unit> {
        return authLocalDataSource.saveAuthToken(token)
    }
}