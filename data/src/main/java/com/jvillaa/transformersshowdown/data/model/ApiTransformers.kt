package com.jvillaa.transformersshowdown.data.model

/**
 * @author Juan Camilo Villa
 *
 * Transformer list data model defined by the server documentation
 */
data class ApiTransformers(
    val transformers: List<ApiTransformer>
)