package com.jvillaa.transformersshowdown.data.datasource.remote.auth

import com.jvillaa.transformersshowdown.domain.helpers.Result

/**
 * @author Juan Camilo Villa
 *
 * This interface is the contract for all the possible remote data operations
 * related with Auth
 */
interface AuthRemoteDataSource {
    /**
     * This function retrieves an auth token from the server
     *
     * @return Result of the Auth server response
     */
    suspend fun retrieveAuthToken(): Result<String>
}