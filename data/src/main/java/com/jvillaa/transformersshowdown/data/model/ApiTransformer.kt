package com.jvillaa.transformersshowdown.data.model

import com.google.gson.annotations.SerializedName

/**
 * @author Juan Camilo Villa
 *
 * Transformer data model defined by the server documentation
 *
 * @param id Transformer id, populated only when it comes from server
 * @param name Transformer name
 * @param team Transformer team: A or D
 * @param strength Transformer strength: 1 - 10
 * @param intelligence Transformer intelligence: 1 - 10
 * @param speed Transformer speed: 1 - 10
 * @param endurance Transformer endurance: 1 - 10
 * @param rank Transformer rank: 1 - 10
 * @param courage Transformer courage: 1 - 10
 * @param firepower Transformer firepower: 1 - 10
 * @param skill Transformer skill: 1 - 10
 * @param teamIcon Team icon URL
 */
data class ApiTransformer(
    val id : String?,
    val name : String,
    val team : String,
    val strength : Int,
    val intelligence : Int,
    val speed : Int,
    val endurance : Int,
    val rank : Int,
    val courage : Int,
    val firepower : Int,
    val skill : Int,
    @SerializedName("team_icon")
    val teamIcon : String?
)