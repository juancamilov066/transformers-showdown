package com.jvillaa.transformersshowdown.data.api

import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.data.model.ApiTransformers
import retrofit2.Response
import retrofit2.http.*

/**
 * @author Juan Camilo Villa
 *
 * This interface provides all the network functions
 * related with transformers management
 */
interface TransformerApi {

    /**
     * This function gets all the transformers which belong
     * to a specific authToken
     *
     * @return Response with transformers list
     */
    @GET("transformers")
    suspend fun getCreatedTransformers(): Response<ApiTransformers>

    /**
     * This function saves a transformer into the server
     *
     * @param transformer Transformer previously created by the user
     * @return Response with the created transformer
     */
    @POST("transformers")
    suspend fun saveTransformer(@Body transformer: ApiTransformer): Response<ApiTransformer>

    /**
     * This function updates a transformer previously created by the user
     *
     * @param transformerToUpdate Transformer previously created by the with updated params
     * @return Response with the updated transformer
     */
    @PUT("transformers")
    suspend fun updateTransformer(@Body transformerToUpdate: ApiTransformer): Response<ApiTransformer>

    /**
     * This function deletes a transformer previously created by the user
     *
     * @param id Previously created transformer's id
     * @return Response with the deleted transformer
     */
    @DELETE("transformers/{id}")
    suspend fun deleteTransformersById(@Path("id") id: String): Response<ApiTransformers>

}