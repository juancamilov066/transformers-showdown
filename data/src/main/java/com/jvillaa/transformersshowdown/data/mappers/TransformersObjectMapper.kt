package com.jvillaa.transformersshowdown.data.mappers

import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.domain.entity.DomainTransformer

/**
 * @author Juan Camilo Villa Amaya
 *
 * Object used to map/convert domain objects to api objects and vice versa
 */
object TransformersObjectMapper : Mapper<ApiTransformer, DomainTransformer> {
    /**
     * This function takes an api transformer and converts it to a
     * domain one
     *
     * @param param Api Transformer
     */
    override fun mapApiToDomain(param: ApiTransformer): DomainTransformer {
        return DomainTransformer(
            id = param.id,
            name = param.name,
            team = param.team,
            teamIcon = param.teamIcon,
            strength = param.strength,
            speed = param.speed,
            endurance = param.endurance,
            rank = param.rank,
            courage = param.courage,
            firepower = param.firepower,
            intelligence = param.intelligence,
            skill = param.skill
        )
    }

    /**
     * This function takes an api transformer and converts it to a
     * domain one
     *
     * @param param Domain Transformer
     */
    override fun mapDomainToApi(param: DomainTransformer): ApiTransformer {
        return ApiTransformer(
            id = param.id,
            name = param.name,
            team = param.team,
            teamIcon = param.teamIcon,
            strength = param.strength,
            speed = param.speed,
            endurance = param.endurance,
            rank = param.rank,
            courage = param.courage,
            firepower = param.firepower,
            intelligence = param.intelligence,
            skill = param.skill
        )
    }
}