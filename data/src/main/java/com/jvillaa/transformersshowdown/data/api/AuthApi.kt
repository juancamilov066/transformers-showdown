package com.jvillaa.transformersshowdown.data.api

import retrofit2.Response
import retrofit2.http.GET

/**
 * @author Juan Camilo Villa
 *
 * This interface provides all auth related
 * network operations
 */
interface AuthApi {

    /**
     * This function gets the auth token from server
     * @return Network response with our token
     */
    @GET("allspark")
    suspend fun retrieveAuthToken(): Response<String>
}