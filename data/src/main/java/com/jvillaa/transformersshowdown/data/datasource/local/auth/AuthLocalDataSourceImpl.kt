package com.jvillaa.transformersshowdown.data.datasource.local.auth

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.jvillaa.transformersshowdown.domain.helpers.Constants
import com.jvillaa.transformersshowdown.domain.helpers.Result
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This class describes how to manage the auth local operations
 *
 * @param sharedPreferences Receives sharedPreference to save
 */
class AuthLocalDataSourceImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : AuthLocalDataSource {

    /**
     * This function describes how to save a token
     *
     * @param token Auth token
     * @return Result of the operation
     */
    @SuppressLint("ApplySharedPref")
    override suspend fun saveAuthToken(token: String): Result<Unit> {
        with(sharedPreferences.edit()) {
            putString(Constants.PREF_TOKEN, token)
            commit()
            return Result.Success(Unit)
        }
    }

}