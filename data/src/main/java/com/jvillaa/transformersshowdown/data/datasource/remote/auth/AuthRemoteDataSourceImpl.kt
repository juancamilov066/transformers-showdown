package com.jvillaa.transformersshowdown.data.datasource.remote.auth

import com.jvillaa.transformersshowdown.data.api.AuthApi
import com.jvillaa.transformersshowdown.domain.helpers.Result
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This class describes how to manage the remote auth operations
 *
 * @param authApi Our auth endpoint client
 */
class AuthRemoteDataSourceImpl @Inject constructor(
    private val authApi: AuthApi
) : AuthRemoteDataSource{

    /**
     * This function describes how to retrieve the auth token from server
     *
     * @return Result with token or error
     */
    override suspend fun retrieveAuthToken(): Result<String> {
        try {
            val response = authApi.retrieveAuthToken()
            if (response.isSuccessful && response.body() != null) {
                return Result.Success(response.body()!!)
            }
            return Result.Error(Exception("Error authenticating user"))
        } catch (exception: Exception) {
            return Result.Error(Exception("Error authenticating user"))
        }
    }

}