package com.jvillaa.transformersshowdown.data.helpers

import android.content.SharedPreferences
import com.jvillaa.transformersshowdown.domain.helpers.Constants
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This interceptor is used to attach the auth token
 * to every request that requires it
 *
 * @param sharedPreferences to extract the token
 */
class AuthInterceptor @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : Interceptor {
    /**
     * This function intercepts the request before being sent and only attach
     * the token to all non-auth endpoints
     *
     * @return Normal response of the call
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (!request.url().toString().contains("allspark")) {
            val token = sharedPreferences.getString(Constants.PREF_TOKEN, "")
            request = request
                .newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
        }
        return chain.proceed(request)
    }
}