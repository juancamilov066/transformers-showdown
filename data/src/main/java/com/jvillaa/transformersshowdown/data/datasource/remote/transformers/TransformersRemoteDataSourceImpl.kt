package com.jvillaa.transformersshowdown.data.datasource.remote.transformers

import com.jvillaa.transformersshowdown.data.api.TransformerApi
import com.jvillaa.transformersshowdown.data.model.ApiTransformer
import com.jvillaa.transformersshowdown.data.model.ApiTransformers
import com.jvillaa.transformersshowdown.domain.helpers.Result
import javax.inject.Inject

/**
 * @author Juan Camilo Villa
 *
 * This class describes how to manage the remote transformers operations
 *
 * @param transformerApi The transformer endpoint client
 */
class TransformersRemoteDataSourceImpl @Inject constructor (
    private val transformerApi: TransformerApi
) : TransformersRemoteDataSource {

    /**
     * This function describes how to get transformers associated to a token
     *
     * @return Result with transformers list
     */
    override suspend fun getTransformers(): Result<ApiTransformers> {
        try {
            val response = transformerApi.getCreatedTransformers()
            if (response.isSuccessful && response.body() != null) {
                return Result.Success(response.body()!!)
            }
            return Result.Error(Exception("Error fetching transformers"))
        } catch (exception: Exception) {
            return Result.Error(Exception("Error fetching transformers"))
        }
    }

    /**
     * This function describes how to save a transformer
     *
     * @param apiTransformer Transformer to be saved
     * @return Result with created transformer
     */
    override suspend fun saveTransformer(apiTransformer: ApiTransformer): Result<ApiTransformer> {
        try {
            val response = transformerApi.saveTransformer(apiTransformer)
            if (response.isSuccessful && response.body() != null) {
                return Result.Success(response.body()!!)
            }
            return Result.Error(Exception("Error saving transformer"))
        } catch (exception: Exception) {
            return Result.Error(Exception("Error saving transformer"))
        }
    }

    /**
     * This function describes how to delete a transformer
     *
     * @param id Id of the transformer to be deleted
     * @return Success or error in the operation
     */
    override suspend fun deleteTransformerById(id: String): Result<Unit> {
        try {
            val response = transformerApi.deleteTransformersById(id)
            if (response.isSuccessful) {
                return Result.Success(Unit)
            }
            return Result.Error(Exception("Error deleting transformer"))
        } catch (exception: Exception) {
            return Result.Error(Exception("Error deleting transformer"))
        }
    }

    /**
     * This function describes how to edit a transformer
     *
     * @param apiTransformer Transformer to be edited
     * @return Result with edited transformer
     */
    override suspend fun editTransformer(apiTransformer: ApiTransformer): Result<ApiTransformer> {
        try {
            val response = transformerApi.updateTransformer(apiTransformer)
            if (response.isSuccessful && response.body() != null) {
                return Result.Success(response.body()!!)
            }
            return Result.Error(Exception("Error editing transformer"))
        } catch (exception: Exception) {
            return Result.Error(Exception("Error editing transformer"))
        }
    }

}